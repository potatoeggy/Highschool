# Unit 1: Essential Skills

## Simple Arithmetics
 
### Addition / Subtraction
 | Expression | Equivalent|
 |:----------:|:---------:|
 | $`a + b`$ | $`a + b`$ |
 | $`(-a) + b`$ | $`b - a`$ |
 | $`a + (-b)`$ | $`a - b`$ |
 | $`(-a) + (-b)`$ | $`-(a + b)`$ |
 | $`a - b`$ | $`a - b`$|
 | $`a - (-b)`$ | $`a + b`$ |
 | $`(-a) -(-b)`$ | $`(-a) + b`$|

### Multiplication / Division    
 | Signs | Outcome |
 |:-----:|:-------:|
 | $`a \times b`$ | Positive |
 | $`(-a) \times b`$ | Negative |
 | $`a \times (-b)`$ | Negative |
 | $`(-a) \times (-b)`$ | Positive |

### BEDMAS / PEMDAS
-  Follow ```BEDMAS``` for order of operations if there are more than one operation   

 | Letter | Meaning |   
 |:------:|:-------:|   
 | B / P | Bracket / Parentheses |    
 | E | Exponent |   
 | D | Divison |   
 | M | Multiplication |   
 | A | Addition |   
 | S | Subtraction |    

- <img src="https://ecdn.teacherspayteachers.com/thumbitem/Order-of-Operations-PEMDAS-Poster-3032619-1500876016/original-3032619-1.jpg" width="300">

## Interval Notation
- A notation that represents an interval as a pair of numbers.     
- The numbers in the interval represent the endpoint. E.g. $`[x > 3, x \isin R]`$   
- ```|``` means ```such that```   
- ```E``` or &isin; means ```element of```      
- ```N``` represents **Natural Numbers** $`N = \{x | x \gt 0, x \isin \mathbb{Z} \}`$    
- ```W``` represents **Whole Numbers**  $`W = \{x | x \ge 0, x \isin \mathbb{Z}\}`$
- ```Z``` represents **Integers** $`Z = \{x| -\infin \le x \le \infin, x \isin \mathbb{Z}\}`$
- ```Q``` represents **Rational Numbers**  $`Q = \{ \frac{a}{b} |a, b \isin \mathbb{Z}, b \neq 0 \}`$  

  | Symbol | Meaning |
  |:------:|:-------:|
  | $`(a, b)`$ | Between but not including $`a`$ or $`b`$, you also use this for $`\infty`$|
  | $`[a, b]`$ | Inclusive |
  | $`a ∪ b`$ | Union (or) |
  | $`a ∩ b`$ | Intersection (and) |

## Pythgorean Theorem
- a and b are the two legs of the triangle or two sides that form a 90 degree angle of the triangle, c is the hypotenuse        
- $`a^2+b^2=c^2`$     

- <img src="http://www.justscience.in/wp-content/uploads/2017/05/Pythagorean-Theorem.jpeg" width="400">
 
## Operations with Rationals
- $`Q = \{ \frac{a}{b} |a, b \isin \mathbb{Z}, b \neq 0 \}`$     
 
- Any operations with rationals, there are 2 sets of rules     
   1. ```Rules for operations with integers```
   2. ```Rules for operations with fractions```   

- To Add / subtract rationals, find common denominator and then add / subtract numerator   
- To Multiply rationals, first reduce the fraction to their lowest terms, then multiply numerators and denominators   
- To Divide rationals, multiply them by the reciprocal   

### Example Simplify Fully:

- $` \dfrac{3}{4} \div \dfrac{2}{14} `$ Reduce to lowest terms

- $` \dfrac{3}{4} \div \dfrac{1}{7} `$ Multiple by reciprocal

- $` \dfrac{3}{4} \times 7 `$

- $` = \dfrac{21}{4}`$ Leave as improper fraction


### Shortcut for multiplying fractions
- cross divide to keep your numbers small    
- Example:    
- $` \dfrac{3}{4} \times \dfrac{2}{12} `$
 
- $` \dfrac{1}{2} \times \dfrac{1}{4} `$
 
- $` = \dfrac{1}{8} `$


## Exponent Laws

 | Rule | Description| Example |
 |:----:|:----------:|:-------:|
 |Product|$`a^m \times a^n = a^{n+m}`$|$`2^3 \times 2^2 = 2^5`$|
 |Quotient|$`a^m \div a^n = a^{n-m}`$|$`3^4 \div 3^2 = 3^2`$|
 |Power of a Power|$`(a^m)^n = a^mn`$|$`(2^3)^2 = 2^6`$|
 |Power of a Quotient|$`(\dfrac{a}{b})^n = \dfrac{a^n}{b^n}`$|$`(\dfrac{2}{3})^4 = \dfrac{2^4}{3^4}`$|
 |Zero as Exponents|$`a^0 = 1`$|$`21^0 = 1`$|
 |Negative Exponents|$`a^{-m} = \dfrac{1}{a^m}`$|$`1^{-10} = \dfrac{1}{1^{10}}`$|
 |Rational Exponents|$`a^{\frac{n}{m}} = (\sqrt[m]{a})^n`$|$`16^{\frac{5}{4}} = (\sqrt[4]{16})^5`$|

**Note:**    
-  Exponential Form --> Expanded Form   
-  6<sup>4</sup> = 6 &times; 6 &times; 6 &times; 6

## Scientific Notation
-  They convey accuracy and precision. It can either be written as its original number or in scientific notation: 
- 555 (**Exact**) or $`5.55 \times 10^2`$ (**3 significant figures**).   
- In scientific notation, values are written in the form $`a(10^n)`$, where $`a`$ is a number within 1 and 10 and $`n`$ is any integer. 
- Some examples include the following: $`5.4 \times 10^3, 3.0 \times 10^2`$, and $`4.56 \times 10^{-4}`$.    
- When the number is smaller than 1, a negative exponent is used, when the number is bigger than 10, a positve exponent is used    

- <img src="https://embedwistia-a.akamaihd.net/deliveries/d2de1eb00bafe7ca3a2d00349db23a4117a8f3b8.jpg?image_crop_resized=960x600" width="500">     

- **Remember**: For scientific notation, round to ```3 significant``` digits

## Rates, Ratio and Percent
- ```Ratio```: A comparison of quantities with the same unit. These are to be reduced to lowest terms.      
- Examples: ```a:b, a:b:c, a/b, a to b ```    

- ```Rates```: A comparison of quantities expressed in different units.      
- Example: ```10km/hour```    

- ```Percent```: A fraction or ratio in which the denominator is 100    
- Examples: ```50%, 240/100```    

## Number Lines
- a line that goes from a point to another point, a way to visualize set notations and the like   
- <img src="https://i2.wp.com/mathblog.wpengine.com/wp-content/uploads/2017/03/numberlines-thumbnail.jpeg?resize=573%2C247&ssl=1" width="500">   
- A solid filled dot is used for ```[]``` and a empty dot is used for ```()```


## Tips
- Watch out for the ```+/-``` signs   
- Make sure to review your knowledge of the exponent laws   
- For scientific notation, watch out for the decimal point 
- Use shortcut when multiplying fractions