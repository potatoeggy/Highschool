# Graduation Test Study Sheet

## Terms
- **Full Disclosure**: The date by which you must drop a course so that it does not show up on your transcript.
- **Dual Credits**: A way to earn a credit in high school and at college.
- Grade 9 and 10: Academic, Applied, Open and Locally Developed
- Grade 11 and 12: University, College, Mixed, Open and Workplace
- **Pre-requisite**: A course that you must take prior to moving on to the next level
- **Transfer Course**: Half a credit. Offered in some boards in the summer. Will allow you to transfer from Applied to Academic in Grade 9 going to Grade 10.
- There are other options besides day school for earning credits (i.e summer school, night school)
- Volunteering in a profit making company is **not acceptable** for earning 40 hours of community service even if you are not being paid
- **CCS Credit Counselling Summary** which shows all credits earned, community hours completed, and whether you passed or failed literacy test. (different from transcript)
- `34 Credit Threshold` – a student can earn as many courses as they want up until graduation. After a student has graduated, they may only earn up until 34 credits. 
- **Dual Credits** - A course taken in college. Offered by the school board. No cost. Called Dual because student receives a credit at both college and high school

## Graduation requirements MUST KNOWS

- **30** credits are needed inorder for you to graduate. 18 compulsory and 12 elective credits

### Compulsory Credits Needed
- Students must earn the following compulsory credits to obtain the Ontario Seconday School Diploma

|Compulsory Credits Needed|Course|
|:------------------------|:-----|
|4|Credits in `English` (1 credit per grade) \*|
|3|Credit in `Mathematics` (1 credit in Grade 11 or 12)|
|2|Credits in `Science`|
|1|Credit in `Canadian History`|
|1|Credit in `Canadian Geography`|
|1|Credit in the `Arts (music, drama, art)`|
|1|Credit in `Health and physical education`|
|1|Credit in `French` as a second language|
|0.5|Credit in `career studies`|
|0.5|Credit in `Civics`|

- Plus **one credit** from each of the follwing groups

|Group|Courses|
|:----|:------|
|Group 1|- English or French as a second lanuage\*\* <br>- a Native language<br>- a social sciences and humanities<br>- Canadian and world studies<br>- guidance and career education<br>- cooperative education\*\*\* |
|Group 2|- health and physical education<br>- the arts<br>- business studies<br>- French as a second language\*\*<br>- cooperative education\*\*\*|
|Group 3|- science (Grade 11 or 12)<br>- technological education<br>- French as a second language\*\*<br>- computer studies<br>- cooperative education\*\*\*|

- \*  **A maximum of 3 credits** in English as a second language ESL or English Literacy development (ELD) may be counted towards the 4 compulsory credits in English, but the fourth must be a credit earned for a Grade 12 comulsory course
- \*\* In groups 1, 2, and 3, a **maximum of 2 credits** in `French as a second language` can count as compulsory credits, one from group 1 and one from either group 2 or group 3
- \*\*\* **A maximum of 2 credits** in `cooperative education` can count as compulsory credits. 
- The 12 optional credits may include **up to 4 credits** earned through approved dual credit courses

### How To Read Couse Code

<img src="https://files.catbox.moe/uqiiby.png" width="600">

### Additional Requirements
- **OSSLT** (Ontario Secondary School Literacy Test) , if you fail, you will take the **OSSLC** (Ontario Secondary School Literacy Couse), you need to pass **one of them** to graduate.
- **40** hours of community service. Remember, universities **only see** that you have **completed them**
- 12 optional credits (electives)

### Credit Threshold
- Credit Threshold : **34 credits**. If needed, you may earn up to 34 credits within five years. It is to encourage students to graduate within 4 years time
- You continue/go beyond 34 credits by:
    - E-learning
    - Night School
    - Summer School
    - Independent Learning Centres
    - Adult Day School.
- The threshold **does not apply** if
    - If you have an Individual Education Plan (IEP).
    - If you are not enrolled in secondary school
    - To English as a Second Language (ESL) core language courses and/or English Literacy Development (ELD) core language courses (e.g. Course Codes: ESL/ELDAO, ESL/ELDBO, ESL/ELDCO, ESL/ELDDO, ESL/ELDEO.


- `Dual credits`: credits that have 2 roles, where you can get one for your highschool years and one in your university course. Its for students who are struggling. This is also possible in elementary school.
- The compulsory **age** for education in Ontario is **18 years**
- Transcript: A high school transcript is basically a record of your academic accomplishments in high school. It lists every class you took, when you took them, and the grade you received in each class, sometimes along with additional information such as standardized test scores and any honors you received.
- What is `Full Disclosure`? Its when a after a date, your grade 11 and grade 12 marks will be available to universities. This is important, as if you are failing a course, you know when/will drop the course, as you do not want it to be part of your top 6 or for universities to see it.

### E-INFO
- You can find:
    - University location
    - Scholarships and bursaries
    - Residence
    - Food Plans
    - Prerequisite
    - Failed and repeated courses
    - Enrollment

- You **cannot find** college info, or anything outside of Ontraio

- To apply to universities, use `OUAC`. (Ontario University Application Centre)
- To apply colleges, use `OCW`(Ontario Colleges Website)
- Apprentices are organized in `4` sectors
    1. Motor Power
    2. Industrial/Manufacturing
    3. Service
    4. Construction
- You only require **6** `U` or `M`courses for unviersity, not for college
- Every college program requires **English**
- Scholarships are not always automatic and are not always based on grades only
- Universities and colleges do not consider one applicant over another if they have more volunteer hours
- You can find on Ontario Collecges Websites the things you find on E-info, but for colleges
- `OSAP`: Ontario Student Assistance Program, to give loans to students with low-interests to help them pay for their education fees. They assess you and give you the amount that is appropriate. Don't lie to them if you have scholarships
- Trends can definetly affect post-secondary choices, say computer science is popular or is earning alot of money, alot of people might prusue that in their post-secondary education




