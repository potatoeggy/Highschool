## Vocabulary
- `Profusely`: To a great degree; in large amounts.
- `Debonair`: (of a man) confident, stylish, and charming.

## Notes
- Day 6023, The protaginist seems to have an affection to people that know who they are, i.e. people that are a different gender than their biological one.
- Also a common theme to show the lovestruck protagnist is that he keeps saying how far he is away from Rihiannon at the start of almost all the chapters since he met her.
- > I'am forty minutes away from her.
- More devil and reverand poole:
- > IF YOU BELIEVE THE DEVIL IS WITHIN YOU, <br> CLICK HERE OR CALL THIS NUMBER.
- Chapter 6015 It seems that the repeated idea above shows the conlusion, when he ends up in her very body.
- > I wake up, and I’m not four hours away from her, or one hour, or even fifteen minutes. <br> No, Iwake up in her house. <br> In her room. <br> In her body.
- There is a contrast to when the protagnist met Nathan, where it was just a simple love story turned dark rather quickly
- In chapter 6021, it is also a long chapter on a drunk person, who killed her brother, looks like the author is stressing a point or something.
- Major conflict between Justin, and A (in another person's body), and Justin calls Rhiannon a `slut` (diction).
- This line is very interesting. It shows that Rhiannon is quick to say no about A, but is hesitant to say the same thing to Justin, this is shown with the exclmation mark and the period. Also it characterizes Rhiannon, who doesn't love Jusin anymore.
- Also I guess also a character vs character conflict between Rhiannon and A.
- > “I don’t love him!” Rhiannon yells back. “But I don’t love you, either.”