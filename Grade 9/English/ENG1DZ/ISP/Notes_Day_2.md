# Notes Day 2

## Vocabulary / Diction
- `Exude`: Discharge (moisture or a smell) slowly and steadily. (of a person) display (an emotion or quality) strongly and openly.
- `Reverend`: Used as a title or form of address to members of the clergy. A member of the clergy (Informal).
- `Cuticle`: A protective and waxy or hard layer covering the epidermis of a plant, invertebrate, or shell. The outer cellular layer of a hair.
- `Musing`: A period of reflection or thought.
- `Purty` (informal): nonstandard spelling of pretty, used to represent dialect speech.

## Notes
- Chapter 6003, protaganist plans to make moves on Rihianna, has fallen for her.
- Nathan Dalby found the protaganists email, is susipicious, potientally foreshawdowing a hunt for the protaganist.
- Protaganist reveals his name to be just `A`.
- Justin seems to actually care about Rihianna?
- Devil things started to rise.
- Seems the protagnist is overrun by his emotions of love, or is a quick person to take risks.
- > I don’t deliberate. I don’t weigh my options. I just type and hit send.
- The use of email throughout the poem is pretty interesting, the format of it.
- nice personification?
- > nerves are jangling with possibility.
- reptition of the devil is becoming a trend.
- More characterization as the progtagnist starts to realize that he is not the only one with the ability, but is he right though.
- > I knowI’m not the only one.
- More devil idea. Its probably part of the main plot.
- > James does not feel like he was possessed by the devil.
- More characterization, sense of remorse, as its the protagnist first time to spend more than one day in someone else's family
- > But what I really feel is goodbye. I am leaving here, leaving this family. It’s only been two days, but that’s twice what
 I’m used to. It’s just a hint—the smallest hint—of what it would be like to wake up in the same place every morning.
 I have to let that go.
- Just realized this book uses alot of POV, with many different identities and personalities shown through the prospective of the person that the protagnist is in, for example, this depressed girl.
- > Kelsea Cook’s mind is a dark place. Even before I open my eyes, I know this. Her mind is an unquiet one, words and thoughts and impulses
constantly crashing into each other. My own thoughts try to assert themselves within this noise. The body responds by breaking into a sweat. I try to
remain calm, but the body conspires against that, tries to drown me in distortion.
It is not usually this bad, first thing in the morning. If it’s this bad now, it must be pretty bad at all times.
- Metaphors plus more POV
- > Depression has been likened to both a black cloud and a black dog. For someone like Kelsea, the black cloud is the right metaphor. She is
surrounded by it, immersed within it, and there is no obvious way out. What she needs to do is try to contain it, get it into the form of the black dog. It
will still follow her around wherever she goes; it will always be there. But at least it will be separate, and will follow her lead.
- This book uses alot of POV for the reader to not only know the character the protagnist is in well, but to show a different aspect of society, through different lenses. 