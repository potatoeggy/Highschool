# Notes Day 1

## Vocabulary
- `Periphery`: The outer limits or edge of an area or object. A marginal or secondary position in, or part or aspect of, a group, subject, or sphere of activity.
- `Tentative`: Not certain or fixed; provisional. Done without confidence; hesitant.
- `Mellower (mellow)`: (of a person's character) softened or matured by age or experience. (especially of sound, taste, and color) pleasantly smooth or soft; free from harshness.
- `Berate`: scold or criticize (someone) angrily.
- `IM'd (informal)`: Past tense of instant message, basically sending someone a instant message or direct message.
- `Chastise`: rebuke or reprimand severely.
- `Rebuke`: Express sharp disapproval or criticism of (someone) because of their behavior or actions.
- `Exhilarated`: Very happy, animated, or elated.

## Page 1-20:
- Main protaginist, who is unknown, somehow has the ability to be in another person's body, for a day. 
- His ability has some traits:
    - Only people that are the same age as him, are the people that he *possess*.
    - He can access facts of the people he is in, but not feelings.
- The protaginist has been like this for the past 16 years, show evident in this line: 
    - > Sixteen years is a lot of time to practice. I don’t usually make mistakes.
- Background of the protaginist
    - He's hurt people with his unknown *power* before. 
    - > I’ve harmed people’s lives in the past, every time I slip up, it haunts me. So I try to be careful.
    - A loner? With interesting ideals. 
    - > I am a drifter, and as lonely as that can be, it is also remarkably freeing. I will never define myself in terms of anyone else. I will never feel the
pressure of peers or the burden of parental expectation. I can view everyone as pieces of a whole, and focus on the whole, not the pieces. I have
learned how to observe, far better than most people observe. I am not blinded by the past or motivated by the future. I focus on the present,
because that is where I am destined to live.
- He's now in Justin's body, talking to his girlfriend, Rhiannon, and is different than Justin, which is mean and instead is nice to her.