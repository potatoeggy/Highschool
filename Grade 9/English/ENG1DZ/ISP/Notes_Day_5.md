## Notes
- In chapter 6023, it is another long chapter about a transgendered person.
- In chapter 604, its is also a long chpater about someone's death.
- Throughtout the book, I feel like the author is showing as a different side of society, the dark side.
- More characterization of A
- > I try to pretend this is my life. I try to pretend these are my parents. But it all feels hollow, because I know better.
- Chapter: 6028, where A meets Nathan and the pope. Very nice atmosphere, the kind where on the outside, its all sunshine and rainbows, but on the inside is just evil. Great contrast also.
- Great use of tension, character v.s character.
- > “Get off of me,” I say, standing up. <br> He seems amused. “I’m not touching you. I am sitting here, having a conversation.” <br> “Get off of me!” I say louder, and start ripping at my own shirt, sending the buttons flying. <br> “What—” <br> “GET OFF OF ME!”
- Strong metaphor, that shows the real evil of the pope. I also assiociate imagery of red eyes, as murder links to blood, which links to red eyes.
- > Poole standing now with murder in his eyes.
- Theme? Foreshadowing?
- > “There’s no point in running away!” Poole yells. “You’re only going to want to find me later! All the others have!”
<br> Trembling, I turn up the radio, and drown him out with the sound of the song, and the sound of me driving away.
- Chapter 6033, which shows the final interaction between A and Rhiannon. A lovely atmosphere, with many allusions to common lovers in society. Heartbreak ending.
- > “I want to fall asleep next to you,” I whisper. <br>  “I love you,” I tell her. “Like I’ve never loved anyone before.” <br> Times moves on. The universe stretches out. I take a Post-it of a heart and move it from my body to hers. I see it sitting there.
I close my eyes. I say goodbye. I fall asleep.
- Final characterization: last line can mean alot of things.
- >I wake up two hours away, in the body of a girl named Katie.
Katie doesn’t know it, but today she’s going far away from here. It will be a total disruption to her routine, a complete twist in the way her life is
supposed to go. But she has the luxury of time to smooth it out. Over the course of her life, this day will be a slight, barely noticeable aberration.
But for me, it is the change of the tide. For me, it is the start of a present that has both a past and a future.
For the first time in my life, I run.
- I like how the author uses days as his chapters, 6000 / (365) = 16...., so it indirectly tells the reader how old the protagnist is.
- Also in this form, its like a diary, which also symoblizes the past, in my opinoin, he's telling us his stories from the future.