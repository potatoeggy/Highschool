# Final Exam Study Sheet
- Why we study civics: We study civics so we can make informed decisions and topics regarding politics, and we also learn more about our own rights, freedoms, responsibilites, and the government.
- `nature of dictatorship`: Someone that rules the nation, that is nor a king or queen, usually a military person. They are often greedy and end up in ruins?
-  Monarchy v.s dictatorship: one is when someone is a heridietary queen or king, one is just a normal person, but usually a military personel
- `majority rule:` The principle that the opinion of the greater number should prevail
- `consensus:` A generally accepted opinion discussion among a group of people
- `Rule of Law:` the restriction of the arbitrary exercise of power by subordinating it to well-defined and established laws.
- `Common good:` the benefit or interests of all, in this case, things related to like welfare and healthcare.
- The `legistative branch` prepares the national budget
- **3 readings** are needed for a bill to be passed into a law
- **The Constitutional Act 1982** was the act that introduced the **charter of rights and freedoms**
- Governments that have a **constitutional monarchy**, include: `Sweden, England, Canada`.
- `Riding (consituency):` The body of works on a specified area who elect a representative to a legistative body
- `Party Platform:` The proposals put forward by a political part during an election campaign
- `Global Village:` Linking the worlds people economically, culturally and politically through the use of technology and other tools
- **Number of seats** in the house of commons: **338**
- Where do MP's Sit: probably 4-5? Its either the government members or opposition members is where they sit
- Responsiblity Of The **Governor General**: Is to represent the **Queen**, and also sign bills with the **royal assent** for the bills to be passed as laws. Plus other political and federal responsiblities.
- A wrong against society, this is considered to be as a **criminal** violation.
- `Think globally, act locally`:  urges people to consider the health of the entire planet and to take action in their own communities and cities. Long before governments began enforcing environmental laws, individuals were coming together to protect habitats and the organisms that live within them. These efforts are referred to as grassroots efforts. They occur on a local level and are primarily run by volunteers and helpers.
- `Sweat shops:` shops run by child labour and slavery?
- **Founders** of `Free the children organization`:  Marc and Craig Kielburger
- **Reasons** for creation of the **UN**: human rights violations during the second world war
- `Genocide:` The deliberate and systematic murder of a cacist or cultural group
- `NGOs:` non governmental non-profit organizations, examples include `Doctor without borders`, `Caveats` `World Wildlife Fund`
- `Apartheid: `Seperation of white and black people in south Africa
- `Orgins of democracy:` in ancient greek, 5th century BCE
- Who can introduce bills in the house of commons? Typically `Cabinet Ministers` and `Parliamentary Secretaries`, but `individual parliamentarians` can also introduce bills (called `Private Member's Bills`).
- Level of government responsible for health care: **provincial**
- Electing the speaker of the house: The Speaker of the House of Commons is the presiding officer of the lower house of the Parliament of Canada and is elected at the beginning of each new parliament by fellow Members of Parliament.
- `Abritration`: an abritrator comes in (third party) and makes a decision that resolves the conflict.
- `Globalization:` the process by which businesses or other organizations develop international influence or start operating on an international scale. 

## 5 Centrals Beliefs To Democracy
1. Citizens should have a voice in decision making
2. All citizens should be treated as equal
3. All citizens should have fundamental rights and freedom
4. Citizens should have a sense of responsibility to other people in the community
5. Citizens should have a sense of what is socially just

## Rights And Freedoms
- `right:` A right is a moral or legal entitlement to have or do something.
- `freedom:`: Freedom is the quality or state of being free, i.e., it is the absence of necessity, coercion, or constraint in choice or action

|Right|Freedom|
|:----|:------|
|Legal and moral entitlements|Absence of necessity, coercion, or constraint, in choice or action|
|Protected by the law|Protected by rights|
|Entitle you to freedom|Granted by rights|

## Autocratic Leadership Style

|When Effective|When Ineffective|
|:---------|:------------|
|- Time is limited<br>- Individuals/group lack skill and knowledge<br>- Group does not know each other|- Developing a strong sense of team is the goal<br>- Members have some degree of skill/knowledge<br>- Group wants an element of spontaneity in their work|

## Responsiblities of Citizens
- Citizens have the responsiblities that come with their rights, like to drive safely given the right to drive, responsiblities in Canada include:
- To respect the rights and freedoms of others
- To obey Canada's laws
- To participate in the democratic process
- To respect Canada's 2 official languages and multicultural heritage

## Requirements Of A Democratic Society
- a form of government in which all eligible citizens have an equal say in the decisions that affect their lives. Democracy allows eligible citizens to participate equally—either directly or through elected representatives—in the proposal, development, and creation of laws.
- An election is hold every 4-5 years, or when the governor general deems it needed


## Executive Branch 
- Branch of governement that has the power to carry out the palns & policies of the government

## Conflict Resolution

|Type|Description|Pros|Cons|
|:---|:----------|:---|:---|
|Physical Force|Power, violence or pressure direct against a person consisting in physical act|Fast, gets the job done|Violence and huge losses from both sides|
|Negotiation|Discussion aimed at reaching an agreement|Peaceful|Very slow, since no one wants the worse side of the arguement|
|Mediation|Intervention in a dispute in order to resolve it|Somewhat peaceful, the conflict is usually solved quickly|May be bad, as someone may get the worse side|
|Arbitration|The process of solving an arguement between people by helping them agree to an acceptable solution|Usually peaceful, and both sides are usually happy|Takes longer than mediation, and physical force can still be involved|
|Consensus|A generally accepted opinion discussion among a group of people|Usually peaceful, both sides are usually happy|Takes a long time, lots of arguing, its hard to come to a common agreement|

## Judicial Branch Role In Settling Penalties
- Branch of governement with power to interpret the law, decide who has broken the law, and what penalties should apply.
- They judge. simply put.

## Make Up Of The Executive Branch
- Queen (represented by the Governor General) $`\rightarrow`$ Prime Minister $`\rightarrow`$ Cabinet $`\rightarrow`$ Public Service
    
<img src="https://files.catbox.moe/3fs684.jpeg" width="500">

## First Past The Post
- `First Past The Post / FPTP:` the candiate who has more votes than any other single candidate win the the election (**popular vote** is basically the same thing)
    - Benefit: Produces more majority than minority governments
    - Problem: Produces results that our increasingly at odds with voters desires.

## Levels Of Government

|Level Of Government|Description|Responsibilities|
|:------------------|:----------|:---------------|
|Municipal|- is a type of local council authority that provides <br>local services, facilities, safety and infrastructure for communities|- Libraries<br>- Snow Removal<br>- Transit<br>- Building Permits<br>- Property Taxes<br>- Water<br>- Waste Management|
|Provincial|- is responsible for areas listed in the Constitution Act, 1867, <br> such as education, health care, some natural resources,<br> and road regulations. <br>- Sometimes they share responsibility with the federal <br> government|- Schools<br>- Education<br>- Health Care Delivery<br>- Social Assistance<br>- Natural Resources<br>- Licences|
|Federal|- This level of government deals with areas of <br>law listed in the Constitution Act, 1867 and that <br>generally affect the whole country.|- National Security<br>- Defence<br>- Military<br>- International Relations<br>- Citizenship & Immigration<br>- Money<br>- Banking<br>- Postal Service|

## Things Global Citizens Can Do To Change The World
- Join An NGO
- Writing letters to governments
- Signing petitions
- Funding people, donating voluntering

## Cases tried in Civil Court
- In these cases, a person or company asks a judge to settle a civil problem, such as
    - a problem concerning an inheritance,
    - a problem involving a contract, or
    - a family problem, such as divorce or custody of children.
- money and debts.
- property.
- housing – such as eviction, foreclosure or to fix bad living conditions.
- an injury – such as from a car accident, medical malpractice or environmental harm.
- marriage and children – such as divorce, child custody, child support, or guardianship

## Obstacles To Democracy
- Ethinic differences and conflicts
- A large gap between the rich and the poor
- a low literacy rate

## Cases tried in Criminal Cases
- The reason these cases come to court is always the same: a person is taken to court because she is accused of a crime.
- assault,
- murder,
- sexual assault,
- identity theft.

## Appeals Process
<img src="https://www.justice.gc.ca/eng/csj-sjc/just/img/CanadaCourtSystem-ENG.jpg" width="800">

## Canadian Political Parties

|Left Wing|Centre|Right Wing|
|:--------|:-----|:---------|
|- more liberal||- more conservative|
|- support change in order to <br>improve welfare of all <br>citizens|- tradition is important but <br>change is supported if most <br>people want it|- tradition is important and <br>change should be treated <br>with caution|
|- government should play <br>a larger role in people's lives <br>(social services, benefits|- government should play <br>a role only in that it improves <br>the lives of citizens|- government should play <br>a small role<br>- private business should <br>ensure needs of citizens are <br>met|
|- law and order are <br>important to protect right of <br>all citizens fairly and <br>equally|- law and order are <br>important to encourage and <br>protect right of invididuals|- emphasis of law and order <br>to protect soceity and its <br>traditions|
|- more freedom to <br>individuals and less power <br>to police||- less freedom to individuals <br> and more power to police|

<img src="https://bodwell.edu/r_smith/Images/PoliticalWings.jpg" width="500">

## Civil Law
- Civil law deals with disputes between private parties or or negligent acts that cause harm to others.
- Common civil suits include disputes about:
    - The terms of a contract
    - Damage or injuries
    - Property
    - Reputation
