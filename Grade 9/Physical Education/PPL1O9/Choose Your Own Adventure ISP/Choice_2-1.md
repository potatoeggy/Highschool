## Stephanie Talks About The Negatives Of Drugs

## Scenario

- Stephanie continues to go on about the negative effects of the drugs. She is able to convince Kevin not to take the drugs.

<img src="https://www.vertical-leap.uk/wp-content/uploads/2018/05/thumbs-down-1400x800.jpg" width="500">

<img src="https://www.connectyourmeetings.com/wp-content/uploads/sites/13/2017/08/Web_sayingNo.png" width="500">

- Kevin and Stephanie leave Kevin’s friend behind and go to enjoy the concert. 

<img src="https://www.kclimo.com/wp-content/uploads/2018/06/shutterstock_612941954-1080x675.jpg" width="500">

- Later that night at Kevin’s place, Kevin realizes that Stephanie is a virgin and wants to have sex with her. 

<img src="http://www.kkmann.com/wp-content/uploads/2015/09/night01_View01b.jpg" width="500">

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfyO3yF-OCjFvwueeFVMiirLrhnT-MgG4WGsdZBq53l7PBpAIv" width="500">


## Choices

- How should Stephanie respond?
- [She should Have Sex Right Away](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-1-1.md)

- [She should Get Protection Then Have Sex](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-1-2.md)