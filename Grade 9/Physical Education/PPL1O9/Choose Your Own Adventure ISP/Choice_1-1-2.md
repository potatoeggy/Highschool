## Stephanie Goes To People She Trusts And Talks With Them

## Scenario
- Stephanie goes right away to tell the people she can trust. She tells her parents and her closest friends about what happened. 

<img src="https://www.ypad4change.org/wp-content/uploads/2017/09/636120872757539984-1330483037_family.jpg" width="500">

<img src="https://www.scarymommy.com/wp-content/uploads/2016/02/Back-in-the-Day.jpg?fit=700%2C400" width="500">

- Together, they fight the situation and tell the principal. 

<img src="https://www.rd.com/wp-content/uploads/2016/03/01-13-things-your-kids-principal-wont-tell-you-appointment.jpg" width="500">

- While the bullying is mitigated, there remains a rift between Kevin and Stephanie.

<img src="https://static.highsnobiety.com/thumbor/x9_cTumaV-mc1kSRBUPnENJP09A=/fit-in/480x320/smart/static.highsnobiety.com/wp-content/uploads/2018/05/24110720/how-to-break-up-with-someone-digital-age-main.jpg" width="500">
