## Stephanie Makes A Scene About The Break Up

## Scenario
- Stephanie makes a big scene out of the break up, calling Kevin many names and yelling at him for his stupid decision to take drugs. 

<img src="https://i1.wp.com/www.artsfwd.org/wp-content/uploads/2012/12/conflictcalvinh.jpg?ssl=1" width="500">

<img src="https://i.ytimg.com/vi/gWOMTe_f0mo/hqdefault.jpg" width="500">

- Stephanie, enraged, walks out. There remains bad blood between Kevin and Stephanie.

<img src="https://cdn.thedailymash.co.uk/wp-content/uploads/20190324205513/facebook-angry-emoji-2.jpg" width="500">