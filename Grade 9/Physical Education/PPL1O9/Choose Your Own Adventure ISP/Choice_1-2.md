## Stephanie Does Not Leave

## Scenario
- Stephanie does not want to leave as she is worried about her lasting relationship with Kevin. 

<img src="http://www.macmillandictionaryblog.com/wp-content/uploads/2017/07/Depositphotos_53269083_l-2015-810x540.jpg" width="500">

- Kevin and his friends continue pushing drugs (opioids) on stephanie. 

<img src="https://fee.org/media/21327/drug_deal_hands_pills_pipe_euro_cash_mini.jpg?anchor=center&mode=crop&width=1920&rnd=131370854150000000" width="500">

- Kevin also asks Stephanie to engage in sexual intercourse.

<img src="https://image.freepik.com/free-photo/hands-holding-sex-word-balloon-letters_53876-83263.jpg" width="500">

## Choices
- [Stephanie Decides To Go With Drugs And Sex](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-2-1.md)

- [Stephanie Refuses Sex and Drugs](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_1-2-2.md)