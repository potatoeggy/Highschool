# Unit 2: Chemistry

## Chemistry Vocabulary List

<table class="table" style="max-width:80%">
<tr>
  <th>Word</th>
  <th>Definition (or diagram/translation)</th>
</tr>
<tr>
  <td>Particle Theory of Matter</td>
  <td>Theory that describes the composition and behaviour of matter as being composed of small particles with empty space</td>
</tr>
<tr>
  <td>Matter</td>
  <td>Substance that has mass and occupies space</td>
</tr>
<tr>
  <td>Mechanical Mixture</td>
  <td>A heterogeneous mixture which one can physically separate</td>
</tr>  
<tr>
  <td>Suspension</td>
  <td>A heterogeneous mixture where insoluble solid particles are distributed throughout a fluid, floating freely</td>
</tr>
<tr>
  <td>Alloy</td>
  <td>A combination of 2+ metals</td>
</tr>
<tr>
  <td>Mixture</td>
  <td>A substance that is made up of at least 2 types of particles</td>
</tr>
<tr>
  <td>Qualitative property</td>
  <td>A property of a substance that is not measured and doesn't have a numerical value, such as colour, odour, and texture</td>
</tr>
<tr>
  <td>Quantative observation</td>
  <td>An numerical observation</td>
</tr>
<tr>
  <td>Precipitate</td>
  <td>A solid that separates from a solution</td>
</tr>
<tr>
  <td>Density</td>
  <td>A measure of how much mass is contained in a given unit volume of a substance; calculated by dividing the mass of a sample of its volume <b>(mass/volume)</b></td>
</tr>    
<tr>
  <td>Element</td>
  <td>Element An element is made up of the same atoms throughout, and cannot be broken down further</td>
</tr>
<tr>
  <td>Metal</td>
  <td>a solid material that is typically hard, shiny, malleable, fusible, and ductile, with good electrical and thermal conductivity</td>
</tr>  
<tr>
  <td>Pure substance</td>
  <td>A substance that is made up of only one type of particle</td>
</tr>  
<tr>
  <td>Atom</td>
  <td>The smallest unit of matter found in substances</td>
</tr>  
<tr>
  <td>Solution</td>
  <td>A uniform mixture of 2 or more substances</td>
</tr>  
<tr>
  <td>Colloid</td>
  <td>is substance with small particles suspended in it, unable to be separated by gravity</td>
</tr>  
<tr>
  <td>Emulsion</td>
  <td>A mixture of 2 insoluble liquids, in which one liquid is suspended in the other</td>
</tr>  
<tr>
  <td>Physical Property</td>
  <td>Characteristic of a substance that can be determined without changing the makeup of the substance</td>
</tr>  
<tr>
  <td>Characteristic</td>
  <td>A physical property that is unique to a substance and can be used to identify the substance</td>
</tr>
<tr>
  <td>Periodic Table</td>
  <td>a table of the chemical elements arranged in order of atomic number, usually in rows, so that elements with similar atomic structure (and hence similar chemical properties) appear in vertical columns.</td>
</tr>
<tr>
  <td>Compound</td>
  <td>Compounds are chemically joined atoms of different elements</td>
</tr>
<tr>
  <td>Non-Metal</td>
  <td>A substance that isn’t a metal</td>
</tr> 
<tr>
<td>Physical Change</td>
<td>A change in which <b>the composition of the substance remains unaltered and no new substances are produced</b></td>
</tr>
<tr>
<td>Chemical Change</td> 
<td>A <b>change</b> in the starting substance and the <b>production of ONE or more new substances</b><br> Original substance does not disappear <b>BUT</b> the composition is rearranged</td>
</tr>
<tr>
<td>Molecule</td>
<td>Two or more <b>non-metal</b> atoms joined together</td>
</tr>
<tr>
<td>Diatomic Molecules</td>
<td>Molecules that <b>only</b> consists of 2 elements <br> H O F BR I N C L - hyrodgen, oxygen, fluorine, bromine, iodine, nitrogen, chlorine.</td>
</tr>
<tr>
<td>Ions</td>
<td>A Charged particle, that results from a <b>loss</b> (cation - positve, less electrons) or <b>gain</b> (anion - negative, more electrons) of electrons when bonding</td>
</tr>
<tr>
<td>Electron</td>
<td>Negatively Charged</td>
</tr>
<tr>
<td>Proton</td>
<td>Positively Charged</td>
</tr>
<tr>
<td>Neutron</td>
<td>Neutral Charged</td>
</tr>
<td>Ionic Charge</td>
<td>The <b>sum</b> of the positive and negative charges in a ion</td>
</tr>
<tr>
<td>Covalent Bond</td>
<td>The sharing of electrons between atoms when bonding</td>
</tr>
<tr>
<td>Valence Electrons</td>
<td>Number of electrons on the most outer orbit/shell of the element</td>
</tr>
</table>

## Particle Theory of Matter
1. Matter is made up of tiny particles.
2. Particles of Matter are in constant motion.
3. Particles of Matter are held together by very strong electrical forces.
4. There are empty spaces between the particles of matter that are very large compared to the particles themselves.
5. Each substance has unique particles that are different from the particles of other substances.

## Physical Properties
- A characteristic of a substance that can be determined without changing the composition ("make-up") of that substance
- Characteristics can be determinded using your 5 senses and measuring instruments 
  - smell, taste, touch, hearing, sight
  - scales, tape, measuring meter

## The 3 States Of Matter

|Solid|Liquid|Gas|
|:----|:-----|:--|
|<img src="https://files.catbox.moe/dew5a3.png" width="200">|<img src="https://files.catbox.moe/w6kwzv.png" width="200">|<img src="https://files.catbox.moe/utx6kd.png" width="200">|
|- Holds Shape<br>- Fixed Volume|- Shape of Container<br>- Free Surface<br>- Fixed Volume|- Shape of Container<br>- Volume of Container|

## Qualitative and Quantitative Properties
 |Type|Definition|Example|
 |:---|:---------|:------|
 |Quantitative Property|A property that IS measured and has **```a numerical value```** |Ex. **```Temperature, height, mass, density```**|
 |Qualitative Property|A property that is NOT measured and has **```no numerical value```**|Ex. **```Colour, odor, texture```**|

## Density
- **The amount of mass (or matter) in a certain volume**
- It is a **quantative** physical property
- For any given substance, density is a **characteristic physical property**.

<img src="https://mathsmadeeasy.co.uk/wp-content/uploads/2017/12/density-mass-volume-triangle.png" width="300">

### Sinking Or Floating?
- Most solids are **more** dense than liquids **(except water!)**
- When you place a solid object inside a liquid
    - It will **sink** if the object is **more** dense than the liquid
    - It will **float** if the object is **less** dense than the liquid

### Strange Behaviour of Water
- Its clear, odourless, tasteless, freezes at O<sup>o</sup>C, boils at 100<sup>o</sup>C
- `Its solid form floats on its liquid form!`
    - `Water particles are different`
    - Due to their `shape` and the way in which the `particles` are arranged, forces it to take up `more space` when packed.
    - Its density is the highest at 4<sup>o</sup>C, where its in liquid form.

### Graphs
- To calculate the density on a graph, simply find the slope of any 2 points on the line in the graph

<img src="http://www.chemistryland.com/CHM151Lab/Lab02Density/Graph.jpg" width="300">

## Quantitative Physical Properties
 - **```Density```**: amount of ```stuff``` (or mass) per unit volume (g/cm<sup>3</sup>)
 - **```Freezing Point```**: point where water solidifies (0<sup>o</sup>C)
 - **```Melting Point```**: point where water liquefies (0<sup>o</sup>C)
 - **```Boiling Point```**: point where liquid phase becomes gaseous (100<sup>o</sup>C)

## Common Qualitative Physical Properties

 |Type|Definition|Example|
 |:---|:---------|:------|
 |Lustre|Shininess of dullness<br> Referred to as high or low lustre depending on the shininess||
 |Clarity|The ability to allow light through|```Transparent``` (Glass) <br>```Translucent``` (Frosted Glass) <br>```Opaque``` (Brick)|
 |Brittleness|Breakability or flexibility<br> Glass would be considered as brittle whereas slime/clay are flexible|
 |Viscosity|The ability of a liquid or gas to resist flow or not pour readily through<br> Refer to as more or less viscous|Molasses is more viscous, water is less (gases tend to get"thicker as heated; liquids get runnier)|
 |Hardness|The relative ability to scratch or be scratched by another substance<br> Referred to as high or low level of hardness| Can use a scale (1 is wax, 10 is diamond)|
 |Malleability|the ability of a substance ```to be hammered``` into a thinner sheet or molded|Silver is malleable<br> Play dough/pizza dough is less<br> glass is not malleable|
 |Ductility|the ability of a substance to be pulled into a finer strand|Pieces of copper can be drawn into thin wires, ductile|
 |Electrical Conductivity|The ability of a substance to allow electric current to pass through it<br> Refer to as high and low conductivity|Copper wires have high conductivity<br> Plastic has no conductivity|
 |Form: Crystalline Solid|Have their particles arranged in an orderly geometric pattern|Salt and Diamonods|
 |Form: Amorphous Solid|Have their particles randomly distributed without any long-range-pattern|Plastic, Glass, Charcoal|

## Chemical Property

- A characteristic (property) of a substance that describes its ability to undergo ```changes to its composition to produce one of more new substances. AKA BEHAVIOUR. Everything has one!```
- ```Cannot be determined by physical properties```
- E.g. ability of nails /cars to rust
- Fireworks are explosive
- Denim is resistant to soap, but is combustible
- Baking soda reacts with vinegar and cake ingredients to rise
- Bacterial cultures convert milk to cheese, grapes to wine, cocoa to chocolate
- CLR used to clean kettles, showerheads because it breaks down minerals
- Silver cleaner for tarnished jewellery, dishes because silver reacts with air to turn black.

## Five Aspects Of Chemical Change
<img src="https://files.catbox.moe/4mt740.png" width="600">

## Properties Of Metals And Non-Metals

|Type|Properties|Picture|
|:---|:---------|:------|
|Metals|- Metals are good `conductors` of heat and electricity<br>- Metals are shiny(lustre)<br>- Metals are ductile (can be stretched into thin wires<br>- Metals are malleable (can be pounded into thin sheets)<br>- A chemical property of metal is its reaction with water which reults in corrosion|<img src="https://img.etimg.com/thumb/height-480,width-640,msid-63797457,imgsize-201095/metals-thinkstock.jpg" width="400">|
|Non-Metals|- Non-metals are poor conductors of heat and electricity<br>- Non-metal are **not** `ductile` or `malleable`<br>- Solid non-metals are `brittle` and break-easily<br>- They are `dull`<br>- Many non-metals are gases (one is liquid)|<img src="https://i.pinimg.com/originals/1a/53/bf/1a53bf708c25d9618a433d198a83b2a0.jpg" width="400" height="450">|
|Metalloids|- Metalloids (metal-like) have properties of both metals and non-metals<br>- They are solids that can be `shiny` or `dull`<br>- They `conduct` heat and electricity better than non-metals but not as well as metals<br>- They are `ductile` and `malleable`|<img src="http://www.mine-engineer.com/mining/mineral/Silicon.jpg" width="400">|

## Periodic Table
<img src="https://chem.libretexts.org/@api/deki/files/121316/1.png?revision=1&size=bestfit&width=1300&height=693" width="1200">

### Trends On The Periodic Table
- The first column are the `Alkali metals`.
    - They are shiny, have the consitency of clay, and are easily cut with a knife. 
    - They are the **most reactive** metals.
    - They react violently with water.
    - Alkali metals are **never found as free elements in nature**. They are always bonded with another element.
- The second column are the `Alkaline earth metals`.
    - They are **never found uncombined in nature**.
- The last column are the `Noble gases`.
    - **Extremely un-reactive**. 
- The second last column are the `Halogens`.
    - The **most reactive non-metals**
    - They **react with alkali metals to form salts**.
- The middle parts are the `transition metals`.
    - They are good conductors of heat and electricity.
    - Usually bright coloured.
    - They have properties similar to elements in their same family
    - Many of them combine with oxygen to form compounds called oxides.
- The rows outside the table are the `Inner tranistion metals`.

<img src="https://files.catbox.moe/6522hg.png" width="600">

- The **left** to the **staircase** are the metals and the **right** are the non-metals. The ones touching the **staircase** are the `metalloids`.

<img src="http://www.sussexvt.k12.de.us/science/The%20Periodic%20Table/Periodic%20Trends_files/image002.jpg" width="300"> 

### How To Read An Element
<img src="https://www.classzone.com/books/earth_science/terc/content/investigations/es0501/images/es0501_p6_readinginfo_b.gif" width="400">


## History of The Atom
|Person|Description|Picture|
|:-----|:----------|:------|
|Democritus|All matter can be divided up into smaller pieces until it reaches an unbreakable particle called an ATOM (cannot be cut)<br>He proposed atoms are of diffent sizes, in constant motion and separated by empty spaces||
|Aristole|- Rejected Democritus ideas, believed all matter was made up the 4 elements, it was accepted for nearly 2000 years|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Four_elements_representation.svg/1227px-Four_elements_representation.svg.png" width="500">|
|John Dalton|- Billbard model, atoms of **different elements are different**<br>Atoms are never **created or destroyed**.<br> - Atoms of an element are identical|<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-AsCpeBvgYIQMSWuGCG7-Rdb8z5QC9Jb92jnCO_nYkI4snYG7" width="500">|
|JJ Thomson|- Atoms contain negatively charged electrons, since atoms are neutral, the **rest of the atom is a positevly charged sphere**. <br> - Negatively charged electrons were **evenly distrubuted** throughout the atom.<br> - **Ray cathode experiment** - basically atoms were attracted to a postive end of the tube, so there most be negative charges in the atoms. <br> <br> <img src="https://study.com/cimages/multimages/16/thomsonexperiment2.png" width="300">|<img src="https://www.electrical4u.com/images/march16/1468862016.png" width="500">|
|Ernest Rutherford|- Discovered that the postively charged **nucleus**. <br> - The nucleus was **surrounded by a cloud of negatively charged electrons**<br> - Most of the atom was just space. <br> - **Gold foil experiement**, alpha particles (postively charged) shot at atom, some bounced off at weird angles, so there most be a postively charged thing there. <br> <br> <img src="http://historyoftheatom.files.wordpress.com/2015/02/gold-foil.jpg" width="300">|<img src="http://atomicmodeltimelinervmf.weebly.com/uploads/1/7/9/9/17998887/1823773_orig.jpg" width="500">|
|Niels Bohr|- Discovered that electrons **orbit the nucleus in fixed paths**, each electron has a **definite** amount of energy, further from nucleus = more energy.<br> - Electrons **cannot** jump orbit to orbit or release energy as light going down. <br> - Each orbit can hold a specifc amount of electrons, `2,8,8,8`, useful for the first 20 elements|<img src="https://s3.amazonaws.com/rapgenius/Bohr%20Atom.png" width="500">|
|James Chadwick|- Discovered the neutron, mass of neutron = mass of proton (basically)<br> - Neutral atoms have **equal numbers** of protons and electrons.|<img src="https://01a4b5.medialib.edu.glogster.com/I28dU77RETpL5o21KLw0/media/43/432f51edf42bbf2082e35268160b789a7344a49f/screen-shot-2014-11-15-at-9-10-48-am.png" width="500">|


## Carbon

## Atoms
- Subscripts - tells us how many of the atom are there, for example N<sub>2</sub> means there are 2 nitrongen atoms.
- Use distrubutive property if there are brackets and a subscript, for example, (CO)<sub>2</sub> is equilivant to C<sub>2</sub>O<sub>2</sub>.
- Atoms are stable if they have a full valence shell (noble gases)
- Each family has the same amount of valence electrons as their family number, so `alkali metals` would have 1 valence electron, `alkaline earth metals` will have 2, `halogens will have` 7 and `noble gases` would have 8. 
- They will also have the same amount of protons as their `atomic number`.
- **Number of protons = Number of electrons**.
- **Number of neutrons = mass - atomic number/number of protons**.


## Bohr-Rutherford / Lewis-Dot Diagrams
- **Bohr-Rutherford**
    - Draw nucleus, and draw the apprioate number of orbits.
    - Put number of **protons** and **neutrons** in the nucleus.
    - Draw the correct number of electrons in each orbit
     <img src="https://d2jmvrsizmvf4x.cloudfront.net/LHJtmeuTDVQ4l2uelrkw_imgres.png" width="300">

- **Lewis-Dot Diagrams**
    - Draw element symbol
    - Put the right number of valence electrons around the symbol, perferably in pairs
    <img src="https://qph.fs.quoracdn.net/main-qimg-a7b2f5ac4b313c27d4bac65c1c8f0a30.webp" width="300">    

### Bonding
- To combine 2 atoms, each element wants to be stable. So they each want a full valence shell, (outer shell) so they are stable.
- They can either `gain`, `lose` or `share` electrons in order to become stable.
- Example:
    - Oxygen and Hydrogen, in order to become stable, they all need 8 valence electrons. Hydrogen has 1, oxygen has 6, so we bring in another hyrdogen and we let them share all their electrons, turning into H<sub>2</sub>O, or water.
     <img src="https://www.seaturtlecamp.com/wp-content/uploads/2014/09/water-molecule-2.gif" width="300">

- Use **arrows** to show gaining or losing electrons.
- **Circle** to show sharing of electrons.



## Naming of Ionic Bonds
1. Write cation (metal) first
2. Write anion (non-metal) second
3. Change the ending of the non-metal to ```ide```.

<img src="https://files.catbox.moe/014ff4.png" width="500">

<br>

<img src="https://files.catbox.moe/3nn8kx.png" width="505">

## Decomposition
  - A chemical change used to break compounds down into simpler substances
  - Energy must be ADDED
    - Using electricity
    - Adding thermal energy
 

## Catalyst
- Substance that accelerates a chemical change without being consumed OR changed itself
  

## Uses of Hydrogen Peroxide
- On cuts/scraps
  - Blood has a catalyst = see bubbling O<sub>2</sub>
- Cleans contact lenses
  - Bubbling removes dirt
- Bleaches
  - React with compounds that provide color
  - RESULT = no colour (bleach blond hair/teeth)
