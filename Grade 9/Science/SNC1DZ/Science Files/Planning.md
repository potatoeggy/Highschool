# Planning

## Article - Glyphosate perturbs the gut microbiota of honey bees
- Related to: Ecosystems

### Review
- The article I'm researching shows evidence of chemicals in our pesticides killing bees. As many of you have probably learned in Geography, while pesticides provide insect-free food, it contains many harmful chemicals that do wonders to organisms. The chemical found in some pesticides conatins a chemical that kills a bee's gut bacteria. The bacteria that is inside the bee's gut is essential for its survival, to break down food and such. Without it, the bee is pretty much dead, we humans, contain many bacteria, even more than our cells in our body to survive, what will happen if your bateria are gone? 

### Personal Engagement
- After seeing a video on death of bees, I felt very intereseted in the topic of how bees basically help out every ecosystem to thrive. My personal opinon on this is this is a very serious issue, as our entire living styles are comprimised. Without bees, we lots of crops and algriculture cannot survive and our foods and basic needs will be stripped away from us.
- 

Source: https://www.pnas.org/content/115/41/10305

# Career - Biomedical Engineer

* design and develop medical devices such as artificial hearts and kidneys, pacemakers, artificial hips, surgical lasers, automated patient monitors and blood chemistry sensors
* design and develop engineered therapies (for example, neural-integrative prostheses)
* adapt computer hardware or software for medical science or health care applications (for example, develop expert systems that assist in diagnosing diseases, medical imaging systems, models of different aspects of human physiology or medical data management)
* conduct research to test and modify known theories and develop new theories
* ensure the safety of equipment used for diagnosis, treatment and monitoring
* investigate medical equipment failures and provide advice about the purchase and installation of new equipment
* develop and evaluate quantitative models of biological processes and systems
* apply engineering methods to answer basic questions about how the body works
* contribute to patient assessments
* prepare and present reports for health professionals and the public
* supervise and train technologists and technicians

- Amount of money:
  - 56,770. to 118,730 
Source: https://www.cmbes.ca/about/what-is-a-biomedical-engineer

- you only need a Baccalaureate Degree, which is 4 years in university to become a Biomedical engineer.
- 
