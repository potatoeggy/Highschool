# Unit 4: Physics

## Terms

|Term |Definition|
|:----|:---------|
|Static Electricity | an imbalance of electric charge on the surface of an object|
|Electron | a negatively charged particle  in an atom|
| Proton | a positively charged particle in the atom’s nucleus|
| Electric charge | a form of charge, either positive or negative, that exerts an electric force|
| Neutral charge | an object that has equal numbers of protons and electrons|
| Negatively charged object | an object that has more electrons than protons|
|Positively charged object | an object that has fewer electrons than protons|
| Electric force | the force exerted by an object with an electric charge; can be a force of attraction or a force of repulsion|
| Induced charge separation | a shift in the position of electrons in a neutral object that occurs when a charged object is brought near it|
| Charging by friction | the transfer of electrons between two neutral objects (made from different materials) that occurs when they are rubbed together or come in contact (touch)|
| Electrostatic series | a list of materials arranged in order of their tendency to gain electrons|
| Charging by conduction | charging an object by contact with a charged object|
| Grounding | connecting an object to a large body, like Earth, that is capable of effectively removing an electric charge that the object might have|
| Conductor | a material that lets electrons move **easily** through it|
| Insulator | a material that **does not** easily allow the movement of electrons through it|
| Charging by induction | charging a neutral object by bringing another charged object close to, but not touching, the neutral object|
| Electric discharge | the rapid transfer of electric charge from one object to another|
| Lightning | a bolt of electricity from sky :D|
| Current electricity | the controlled flow of electrons through a conductor|
| Electric circuit | a continuous path in which electrons can flow|
| Switch | a device in an electric circuit that controls the flow of electrons by opening (or closing) the circuit|
| Load | the part of an electric circuit that converts electrical energy into other forms of energy|
| Electrical energy | the energy provided by the flow of electrons in an electric circuit|
| Electric cell | a device that converts chemical energy into electrical energy|
| Fuel cell | ?|
| Direct current (DC) | a flow of electrons in one direction through an electric circuit|
| Alternating current (AC) | a flow of electrons that alternates in direction in an electric circuit|
| Electrical power | the rate at which electrical energy is produced or used|
| Kilowatt-Hour | the SI unit for measuring electrical energy usage; the use of one kilowatt of power for one hour|
| Efficiency | comparison of the energy output of a device with the energy supplied|
| Circuit | a way of drawing an electric circuit using standard symbols|
| Series circuit | a circuit in which the loads are connected end to end so that there is only one path for the electrons to flow|
| Parallel circuit | a circuit in which the loads are connected in branches so that there are two or more paths for electrons to flow|
| Potential difference (voltage) | the difference in electrical potential energy per unit charge measured at two different points; measured in volts|
| Voltmeter | a device used to measure potential difference (voltage)|
| Electrical resistance | the ability of a material to oppose the flow of electric current; measured in ohms|
| Ohmmeter | a device used to measure resistance|
| Resistor |a device that reduces the flow of electric current|
| Ohm’s law |the straight line relationship between voltage and current; `R = V/I`|
| Electrolyte | Anything that conducts electrons, usually a liquid solution|


## Static Charges
- `Static Charges`: An unequal number of individual electric charges on the surface of an object.
- Basically, the charges are **nearly fixed** on the **surface** of an object.
- `Net Charge`: The total electrical charge of an object, in other words, the difference between the protons and electrons.

- An object is:

|Charge|Condition|
|:-----|:--------|
|Positively Charged|When number of `electrons` are **less than** the number of `protons`|
|Neutrally Charged|When the number of `electrons` are **the same** as the number of `protons`|
|Negatively Charged|When the number of `electrons` are **more than** the number of `protons`|

- Charge is basically the difference between the amount of protons and electrons, the greater the difference, the greater the charge.
- A greater charge between objects is determined by the amount of difference between the protons and electrons in each of the objects. The one with more difference has a greater charge.

## Triboelectric Series / Electrostatic Series
- Different materials have different abilities to hold onto electrons
- `Triboelectric Series`: A list of material that is arranged according to their ability to hold on to electrons.

### Table Of Tribolecetric Series Of Common Materials
<img src="https://files.catbox.moe/i7ez1v.png" width="500">

## Law Of Electric Charges
- `Electric Charges`: A charged object has the ability to exert a force (ability to exert energy)
    - This charges can either be **Attractive force** (pulling together) or **repulsive force** (pushing apart).

1. Objects that have like charges **REPEL** each other.
2. Objects that have opposite charges **ATTRACT** each other.
3. A neutral object is **sometimes attracted** to a charged object.

## Charging Statics
- There are **3 methods** in charging a neutral object
1. Charging by Fricion
2. Charging by Contact
3. charging by induction

- There are **3 methods** in discharging a charged object
1. Ground
2. Discharge
3. Time


### Charging By Friction
- When 2 objects of different materials rub against one another, the electrons moves between the objects, one loses electrons and the other gains electrons. 
- The object that gains electrons becomes negatively charged, and the other becomes positevely charged.

### Charging By Contact
- When two objects of different electric charge touch each other, they will **balance** the charge, by transferring electrons from one object to the other.
- There are alot of different scenarios.
1. When a **postive** touches a **neutral** object.
    - electrons are transferred from the **netural** object to the **postive** object.
2. When a **negative** touches a **neutral** object.
   - electrons are transferred from the **negative** object to the **neutral** object.
3. When a **negative** touches a **negative** object.
    - The object with more **negative charge** (more electrons) will transfer some of its electrons to the weaker charged object.
4. When a **positive** touches a **positive** object.
    - The object with less **positive charge** (more electrons) will share some of its electrons to the object with more protons.
5. When a **positive** touches a **negative** object.
    - The **negative charged** (more electrons) object will transfer its electron to the **positively charged** (less electrons) object.

#### Summary
1. When an **positively charged** object touches a **neutral** object, the electrons in the **neutral** object will more to the **positively charged** object.
2. When an **negatively charged** object touches a **netural** object, the electrons in the **negatively charged** object will more to the the **neutral object**.
3. When 2 **charged** objects touch, the one with **more electrons** will move its electrons to the one with the **lower** charge.

### Charging By Induction
- Charging a neutral object by bringing another charged object close by, but not touching the neutral object.
- There are 2 types of this, one is temporary and one is more permanent.
- `Grounding`: When a object is really big like the Earth, it will remove the charge on the object and return it to neutral. The reason is due to the Earth's size, so it can lose and gain electrons **without any effect**.

### 1. Charging by Induction temporarily: Induced Charged Separation
- The electrons in the charged object get attracted by the positive charges in the netural object, thus pushing away the electrons in the neutral object, in essence, its **charge seperation**.
- The like charges will be repelled from the induction point, and the opposite charges will be attracted to the induction point.
- After the charged object is removed, the object will reutrn neutral. This is why this is a temporary way of charging an object.

### 2. Charging by Induction Permanently: Induced Charge Separation + Ground
- Basically, after being touched by a charged object, the neutral object will have a temporary charge, then the ground removes some of the electrons in the object, and afterwards, the object stays positive.
- If the object is removed before the ground, it goes back to neutral, the ground must be **removed first** in order to permantely charge the object.

## Conductors and Insulators
- A conductor is a type of material that allows electrons to flow easily throught it.
- A insulator is a type of material the has high resistance agains the flow of electrons throught it.

|Example|Type|
|:------|:---|
|Lightning Rod|Conductor|
|Copper|Conductor|
|Silver|Conductor|
|Rubber|Insulator|
|Plastic|Insulator|

## How Lightning Works
- In the cloud, it acts as a giant electrical generator.
- Water droplets rise up and freeze and fall back, they are now either postive or negatively charged.
- The negative charges go to the cloud and the postiive charges move to the top.
- When the charges reunite, lightning will strike.

<img src="http://www.physics-and-radio-electronics.com/blog/wp-content/uploads/2016/10/lightningbetweenuppercloudandground.png" width="500">

## Dangers / Applications of Electrostatics
- **Dangers**:
    - Any sparks are small inginitions in places such as gas stations or oil refineries can cause fires and expolsions.
- **Applications**
    - remove pollutants
    - coat / paint surfaces
    - fabric softener sheets

## Static & Current Electricity
- `Static electricity`: an imblanace of electric charge on the **surface** of an object. Generally, it happens in **insulators**.
    - Example: Static in hair, charging by conduction...
- `Current electricity`: The controlled **flow** of electrons through a conductor. Generally, it happens in **conductors**.
    - Example: Cell phone, computer, washing machine...

### Types of Currents
- There are 2 types of current electricity, **Direct current** and **Alternating Currnet** (ACDC).

### 1. Direct Current
- The electricity flow in which the electrons flow in **one direction** only.
- Example: any device that use battery such as cell phone, IPod, alarm clock, camera, etc.

### 2. Alternating Current
- The electricity flow in which the electrons **repeatedly reverse** flow direction.
- Example: Any device that use electric power outlet such as television, printer, XBOX, etc.

## Electrical Components Of Circuits
- <img src="https://files.catbox.moe/hwymit.png" width="500">
- <img src="https://files.catbox.moe/r68d9o.png" width="500">
### Mandatory Componenets
1. **Energy Source**: `Releases energy` to provide power to other componenets that are connected
    - Example Electrochemical cell, battery, electrical outlet...
2. **Load**: `Converts` electrical energy into other form of energy.
    - Example: Light bulb, heater, fan, motor...
3. **Conductor**: Allows `Electricity to flow` through them easily
    - Example: Copper wire, gold wire, silver wire.

### Optional Items
1. **Switch**: Turns the electric circuit on or off **by cutting off the path**.
2. **Measuring Device**: Measures the `electrical property` of the circuit.
3. **Fuse**: `Safety device` to prevent further damage to other electrical components.


## Batteries
- `Electrochemical Cell`: A package of chemicals that converts `chemical energy into electrical energy`.
- `Battery`: More than 1 cell / two or more electric cells in combination.
- Types of cell: `dry` (paste, like a AA battery), `wet` (liquid, like a Car battery).

### Electrical Cells
- Device that converts chemical energy into electrical energy
    - i.e. **portable devices (cameras, cellphones, flashlights)**
- Consists of:
    - 2 electrodes in a conducting solution (electrolyte)
    - **Electrodes** = conductors (1 easily postively charged and the other easily negatively charged)
- Electrons in the electrolyte are repelled by the negative electrode and attracted to the positive electrode

### 1. Wet Cell (Voltaic Cell)
- <img src="https://files.catbox.moe/krm6sm.png" width="500">

### 2. Dry Cell 
- <img src="https://files.catbox.moe/0wuuml.png" width="500">
- Rechargeable cells are called **Secondary** cells, while non-rechargeable cells are called **Primary** cells.

### 3. Specialized Cells
- **Fuel cell** 
    - Generates using external fuel, works like an engine without a moving part, requires continous source of fuel and oxygen.
    - Operate far longer than a conventional electric cell
    - Generates `non-polluting` exhaust.
    - I.e Hyrdogen fuel cell: produces electrical energy by converting hyrdogen and oxygen into water
    - **Uses**:
        - Primary and backup power for commerical industrial and residential buildings.
        - Power fuel-cell vehicles.
- **Solar Cell**
    - Converts `light` energy into electrical energy.
    - Based on semi-conductor `silicon`.
    - `Very low efficiency` (the best is around 40-50% efficiency)

## Generating Electricity
- `Non-renewable energy source`: energy source that cannot be replaced as quickly as its is used.
- `Renewable energy source`: energy source that can be re-used or replaced quickly.

### Types of Energy
|Type|Example|
|:---|:------|
|Nuclear|Nuclear reaction|
|Gravitational|Ball rolling down a hill|
|Chemical|Chemical reaction|
|Kinetic|Any movement|
|Sound|Voice, music|
|Thermal|Heat from fire|
|Electricity|Lightning, electricity in your home|
|Light|Sunlight, Microwave|

### Pros and Cons

|Method|Advantage|Disadvantage|
|:-----|:--------|:-----------|
|Fossil Fuel|- very low cost to generate electricity|Create pollution during `mining` and `burning` fossil fuels.|
|Biomass|- very low cost to generate electricity <br> - `renewable` compared to fossil fuel|- create pollution during `burning` biomass <br> - can result in lower `food` production|
|Nuclear|- `low cost` to generate electricity <br> - needs very little `fuel`|- very `high cost` to construct and maintain <br>- create `radioactive` waste <br>- at risk during `natural diasters`|
|Hydroelectric|- `low cost` to generate electricity if large scale <br>- no `air` pollution|- huge `disruption` to large area of envirnoment during construction <br>- very `highcost` to construct|
|Tidal|- `low cost` to generate electricity <br>- no `air` pollution|- huge `disruption` to large area of envirnoment during construction <br> - very `highcost` to construct <br>- very `few` suitable location(s)|
|Wind|- `low cost` to generate electricity <br>- Can be built `anywhere` that has wind available|- the wind does not always blow, which means the electricity generated is `not reliable`<br>- potiential danger to `birds`|
|Solar|- Can be built `anywhere` that has sunlight available <br> - no cost on `fuel`<br> - easy to install in `remove` `areas` <br>- no `air` pollution|- Very low `efficiency`, which means a `large` are is needd to get enough sunlight <br> - generally only works during `daytime`|
|Geothermal|- `low cost` to generate electricity <br> - no `air pollution|- very `few` suitable location(s) <br>- very `high cost` to construct|

## Measurement in Electric Circuits

|Electrical Quantity|Electrical Quantity Symbol|Unit of Measurement|Unit of Measurement Symbol|Instrument Of Measurement|Symbol For Insturment|
|:------------------|:-------------------------|:------------------|:-------------------------|:------------------------|:-------------------:|
|     Resistance    |            R             |       Ohms        |             Ω            |         Ohmmeter        |<img src="https://www.wisc-online.com/assetrepository/getfile?id=4674&getType=view&width=0&height=0" width="200">|
|     Current       |            I             |       Amperes     |             A            |         Ammeter         |<img src="https://i.stack.imgur.com/sTYoJ.png" width="200">| 
|     Voltage       |            V             |       Volt        |             V            |         Voltmeter       |<img src="https://upload.wikimedia.org/wikipedia/commons/5/59/Voltmeter_symbol.png" width="200">|
|  Electric Charge  |            Q             |       Coulomb     |             C            |                         ||         

### 1. Electric Charge
- **Symbol** for **Electric Charge**: `Q`.
- **Unit** for **Electric Charge**: `Coulomb [C]`.
- **1C = the charge of** $`6.25 \times 10^{18}`$ **electrons (6 250 000 000 000 000 000)**.

### 2. Electric Current
- The rate (speed) of the movement of electric charge (electrons) in an electric circuit.
- **Symbol** for **Electrical Current**: `I`.
- **Unit** for **Electrical Current**: `Ampere [A]`.
- **1 A = 1 C per 1 s** = 1 coulomb of charge moved pass a point in 1 second
- In an electric circuit, there are 2 convention of how electrons flow in the circuit.
    1. Conventional
        - electrons flows from the `positive` terminal to `negative` terminal.
    2. Electron flow
        - electrons flows from the `negative` terminal to `positive` terminal.
        - Correct way vuit it is not widely used.
- The electric currnet is measured by an `ammeter`.
    - It is connected `in series` to the item you wnat to measure, which means you put the ammeter `infront or behind` the item you want to measure.

### 3. Potential Difference / Voltage
- the `difference` in electrical potential `energy` when a charge moved between `2 points` in an electric circuit.
- **Symbol** for **Voltage**: `V`.
- **Unit** for **Voltage**: `Volt[V]`
- **1V = 1J of energy per 1C of charge** (each coulomb of charge have 1J have energy).
- The **voltage** is measured by a `voltmeter`.
    - It is connected `in parallel` to the item you want to measure, which means you `create a new path` for the voltmeter next to the item you want to measure.

### 4. Electrical Resistance
- The `opposition` to the flow of electric current in a material. This is a property of the `material`.
- **Symbol** for **Electric Resistance**: `R`
- **Unit** for **Resistance**: `Ohms[Ω]`
- The resistance is measure by an `ohmmeter`.
    - It is connected `in parallel` to the item you want to measure, which means you `create a new path` for the ohmmeter next to the item you want to measure.
- #### Reasons for resistance:
    - A `load` convert `electrical energy into other energy`
        - Example: light bulb convert electrical energy to light energy.
    - Setup the `flow` of circuit
        - Example: Resistor adjusts flow of circuit (like narrowing down a road)
    - `Everything` has resistance. Conductor has low resistnace. Insulator has high resistance.
        - Example: wire has an resistnace so low that it is ignored in many calculations.

## Factors Affecting The Resistance of Wires
- ### 1. Type of Material
    - Copper has much less resistance than iron.
    - **Analogy**: Water flowing through a pipeful of gravel. Resistance depends on type of gravel.
- ### 2. Length of Wire
    - `Longer` wires have more resistance.
    - **Analogy**: Water flowing through a pipeful of gravel. Resistance depends on length of pipe.
- ### 3. Thickness 
    - `Narrower` wires have more resistance.
    - **Analogy**: Water flowing through a pipeful of gravel. Resistance depends on diameter of pipe.
- ### 4. Temperature
    - `Higher temperature` gives more resistance (More particles moving around and blocking it).
    - A hot filament has more resistance tahn a cold filament because atoms in the hot filament vibrate more and the vibration increases the resistance.

## Product Efficiencies
- Simply take the output / input x 100 and you get the percentage of the efficiency of your product.

## Ohm's Law
- Ohm's law states that $`V = IR`$, which means the volatage is equal to the current multiplied by the resistance.
- When given a circuit, with one unknown value such as `I`, simply use Ohm's law to find the missing value.

## Series Circuits
- In a series circuit, there are a set of rules where the current, voltage, and resistance follow.
- $`I_T = I_1 = I_2 = I_3 \cdots = I_N`$
- $`V_T = V_1 + V_2 + V_3 \cdots + V_N`$
- $`R_T = R_1 + R_2 + R_3 \cdots + R_N`$
- As **MORE** loads are added:
    - $`R_T`$ `increases` because more loads are added.
    - $`I_T`$ `decreases` because $`R_T`$ increases.
    - $`I_{load}`$ `decreases` because voltage and resistance are the same for each load. 
    - $`V_{load}`$ `decreases` because each load gets `less` voltage ($`V`$) ($`V_T`$ supplied by battery gets divided by more loads).
    - Each light bulb has the `less brightness` **(because brightness depends on voltage)** (Less energy gets converted into heat and light in each light bulb).

## Parallel Circuits
- In a parallel circuit, there are also a set of rules where the current, voltage, and resistance follow. The current and voltage rules are swapped in this case.
- $`I_T = I_1 + I_2 + I_3 \cdots + I_N`$
- $`V_T = V_1 = V_2 = V_3 \cdots = V_N`$
- $`R_T = (R_1^{-1} + R_2^{-1} + R_3^{-1} \cdots + R_N^{-1})^{-1}`$ (I think).
- $`R_T = (R_1 \times R_2)/(R_1 + R_2)`$ (Don't really trust this more, this one is from Alexander Wang, haven't been confirmed with the teacher).
- As **MORE** loads are added:
    - $`R_T`$ `decreases` because more loads are added.
    - $`I_T`$ `increases` because $`R_T`$ decreases.
    - $`I_{load}`$ `stays the same` because voltage and resistance are the same for each load. 
    - $`V_{load}`$ `stays the same` because each load gets the same voltage as before.
    - Each light bulb has the `same brightness` **(because brightness depends on voltage)**.

## Comparing Series And Parallel Circuits

|Series Circuits|Parallel Circuits|
|:--------------|:----------------|
|- An electric circuit in which the compoenents are arranged one after another. <br>- It has only `one` path along which electrons can flow.<br>- If that path is interrupted, the whole circuit `stops`.|- An electric circuit in which the parts are arranged so that electrons can flow along `more` than one path. <br>- An interruption in one path `does not affect` the other path in the circuit|
|**Voltage (V):**<br>- Total voltage is determined by the battery used<br>- Each load uses `portion` of the total voltage supplied by the battery<br>- As the number of loads `increases`, the potiental difference (voltage) for each load `decreases`|**Voltage (V):**<br>- Total voltage is determined by the battery used<br>- Voltage across parallel resistors will always be `the same`, even if the resistors have `different` values<br>- As the number of loads `increases`, the potiental difference (voltage) for each load `stays the same`|
|**Current (I):**<br>- The current is the `same` throughout a series circuit<br>- As the number of loads `increases`, the total current `decreases`|**Current (I):**<br>- Each load uses `portion` of the total current from the battery<br>- As the number of loads `increases`, the total current `increases`|
|**Resistance (R):**<br>- Total resistance `increases` when more resistors / loads are added<br>- As the number of loads `increases`, the total resistance `increases`|**Resistance (R):**<br>- Total resistance `decreases` when more resistors / loads are added<br>- As the number of loads `increases`, the total resistance `decreases`|


