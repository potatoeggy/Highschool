# Unit 1: Chemistry

- `Matter`: has mass, takes up space.
    - fundamental unit -> ATOMS
- One `formula unit` - repeating strucure in an ionic compound that has the simplest ratio of ions in the compound
    - Can be represented in **Chemical Formula** (e.g $`Li_3P`$) 
- Ions are particles with charges
- `Models`: Allows people to make accurate `predictions` about `behavior` of MATTER.
- `Atom`: **Smallest** unit of element that still retains its properties. Made of **subatomic** particles
- `Atomic mass unit:` $`1.67 \times 10^{-27}kg`$


## Atoms
- Atoms are the smallest unit of an element that still retains its properties
- Atoms are made of subatomic particles
- Relative Charge: compared to something. 
- AMU = atomic mass units
- atmoic notation $`_{19}K`$
- an isotope is an atom (or atoms) of an element with a unique # of neutrons

|Name     |Symbol|Relative mass (amu)|Location|Relative Charge|
|:--------|:-----|:------------------|:-------|:--------------|
|Protons  |$`p^+`$|       $`1`$          |nucleus | $`+1`$        |
|Neutrons |$`n^0`$|     $`1`$            |nucleus | $`0`$         |
|Electrons|$`e^-`$|$`~\dfrac{1}{2000}`$|in orbit around nucleus (shell, energy level)|$`-1`$|

## IUPAC
|Letter|Definition|
|:-----|:---------|
|I|International|
|U|Union|
|P|Pure and|
|A|Applied|
|C|Chemistry|

- Involved in studying, varifying information (eg the periodic table -> Describes the elements -> pure susbtances made of only one kind of atom),and publishing.
- Standarize the information for the public

## Bohr Rutherford
- Electrons in **uncharged** atom, # protons $`=`$ # electrons
- Mass of an atom is the weighted average if akk usitioes if element
- `Atomic Notation`, Top number is the mass, bottom number is the atomic number.

<img src="https://d2jmvrsizmvf4x.cloudfront.net/LHJtmeuTDVQ4l2uelrkw_imgres.png" width="300">

## Lewis Structures (dot diagrams)
- shows valence $`e^-`$; centre is atomic symbol
- Use family groups to figure out valence $`e^-`$

<img src="https://upload.wikimedia.org/wikipedia/commons/6/6b/Lewis_dot_K.svg" width="100">
<img src="http://www.chemistrylearner.com/wp-content/uploads/2018/08/Neon-Lewis-Dot-Structure.png" width="100">
<img src="https://qph.fs.quoracdn.net/main-qimg-9a09a4e3ae1808d51ada5405916408e9.webp" width="100">

## Trends on the Periodic Table
- `Periodic Table:` Describes **elements** pure susbatances made of only **1** type of Atom.
- The further away the electron is from the nucleus, the more energy it has.
- `Periods:` repeating pattern.
- Metals on **bottom left**, non-metals on **top right**

### Measuring Atomic Radius
- Stack a bunch of them, measure, divide by number of atoms, easy clap :p.

<table>
<tr>
<th>Trend</th>
<th>You move <b>along a period (row)</b> from <b>left to right</b></th>
<th>you move <b>down a group (column)</b> from <b>top to bottom</b></th>
</tr>

<tr>
<th>Number of <b>valence electrons</b> <br> and electron shells</th>
<td>Valence shells <b>stays the same</b>, while electrons <b>increases</b></td>
<td>Valence shells <b>increases</b>, while electrons <b>stays the same</b></td>
</tr>

<tr>
<th><b>Atomic Radius</b> <br> (size of an atom)</th>
<td>Decrease due to more protons in the nucleus that attract the electrons, while having the same atomic radius</td>
<td>Increases due to shielding and more energy levels, which actually cancels out and is greater than the force of increasing protons in the nucleus</td>
</tr>

<tr>
<th><b>Reactivity of group 1 + 2 metals</b> <br> (i.e How likely are they to lose electrons?)</th>
<td>Decreases due to smaller atomic radius and more protons in the nucleus</td>
<td>Increases due to larger atomic radius</td>
</tr>

<tr>
<th><b>Reactivity of non-metals</b> <br> (Ie. How likely are they to gain electrons?)</th>
<td>More likely to gain electrons, more protons in nucleus and stronger hold on them</td>
<td>More likely to gain electrons, more protons in nucleus and strong hold on them</td>
</tr>
</table>

## Rows
- Same energy level in each period
- Same number of valence electrons in each group
- Across a row/period $`\rightarrow`$ more $`p^+`$ in nucleus $`\rightarrow`$ greater attraction to $`e^-`$
- Atomic radius decreases as you move acroos a row/period, due to more protons in the nucleus that attract the negatives.
- Atomic radius is the from the center of the atom (nucleus) to the outer most shell (valence shell)

## Columns
- down a column, increase of energy level, as you move down
- every atom has only one valence shell (cause its the most outer shell)
- if valence shell is further away from the nucleus, less attractive force between nucleus and valence $`e^-`$
- more energy levels where $`e-`$ can be
- Negative electrons are repeling the valence shell electrons `(shielding)`
- `Shielding` "inner electrons" repel valence electrons and "block" attraction force between valence electrons and nucleus
- Atomic radius increases as you move down a column/group



## Metals
- They tend to lose electrons
- They are shiny, ductile, malleable, conductive
- They have a weak/loose hold on electrons
- Most metals are considered to be multi-metals (`multi-valent`)
    - can form ions of differing charges 
    - add roman numerals to the ions name to indicate its charge, for example, iron($`III`$) oxide.
- `Metalloids`: non-metals with same metallic or metals with non-metalic properies`

## Non-Metals
- They are dull, bad conductors - insulators
- Tend to gain electrons
- The have a strong hold on electrons
- Usually non-ductile nor malleable

## Bonds
- An ionic bond is a bond between a negative ion and a positive ion (so a anion and a cation)
- An convalent bond is a bond between 2 non-metals
- An ion is a charged particle
- An anion is formed when an particle gains electrons
- An cation is formed when an particle loses electrons
- We can use modesl(e.g Lewis dot diagrams) to show bonding
- Atoms will lose or gain electrons to achieve noble gas $`e^-`$ configuration $`\rightarrow`$ The most common stable ion. (eg, if $`Na`$ loses electrons, it becomes like $`Ne`$, if $`Cl`$ gains an electron, it becomes like $`Ar`$)
- To show that atoms are different than ions, we put square brackets around it $`[Na]`$, then we put superscript on the top right to show its charge, $`[Na]^+`$ (if the charge is only a $`\pm 1`$, we just put a $`+`$ instead of $`1+`$)
- Example of ionic bond:
- <img src="https://cdn.kastatic.org/ka-perseus-images/5ea8232d69862ad00d65c8d625907b7e1b04172e.jpg" width="200"> 


## Non-Metal Ionic Names
|Element|Name|
|:------|:---|
|Hydrogen|Hydride|
|Boron|Boride|
|Carbon|Carbide|
|Nitrogen|Nitride|
|Oxygen|Oxide|
|Fluorine|Fluoride|
|Silicon|Silicide|
|Phosphide|Phosphide|
|Sulfur|Sulphide/Sulfide|
|Chlorine|Chloride|
|Arsenic|Arsenide|
|Selenium|Selenide|
|Bromine|Bromide|
|Tellurium|Telluride|
|Iodine|Iodide|
|Astatine|Astitide|

## Chemical Nomenclature
- Naming and writing chemical formuals
- According to IUPAC
- Direct relationship beween chemical name and chemical structure
- - Going down diagonally from `aluminium`, we get a pattern of $`3+`$, $`2+`$, $`1+`$ of charge. `Aluminium` has a charge of $`3+`$, `Zinc` has a charge of $`2+`$, and `silver` has a charge of $`1+`$, and they are all mono-valent. (not multi-valent)
    - `Galvanize` (rust $`\rightarrow`$ white shield $`\rightarrow`$ cover iron $`\rightarrow`$ prevnet rusting, but I don't think it will be in this unit)

|Formula|Name|
|:------|:---|
|$`NaCl`$|Sodium chloride|
|$`K_3P`$|Potassium phosphide|
|$`Mg_3P_2`$|Magnesium phosphide|

## Polyatomic Ions
- Ions that are made of $`\ge 2`$ atoms.
- Molecules with a charge
- eg. $`CaCo_3`$
    - $`Ca \rightarrow`$ Calcium ion $`Ca^{2+}`$ `(Cation)`
    - $`CO_3 \rightarrow`$ Carbonate ion $`CO_3^{2-}`$ `(Anion)`
    - `Calcium carbonate`
- The ones that are not multi-valent are:
    - The first `20` elements
    - `alkali metals`
    - `alkaline earth metals`
    - non-metals (the ones hugging the staircase are also non-metals (some of the `metalloids`))
    - `halogens`
    - `noble gases`
- If there is more than one polyatomic ion in a formula unit, then surround the ion with brackets/parentheses
- Oxyanion are negative ions with oxygen in them

|Polyatomic Ion Name|Formula (Always Remember The Charge!)|
|:------------------|:------------------------------------|
|Ammonium|$`NH_4^+`$|
|Acetate|$`CH_3COO^-`$|
|Borate|$`BO_3^{3-}`$|
|Chlorate|$`ClO_3^-`$|
|Cyanide|$`CN^-`$|
|Hydroxide|$`OH^-`$|
|Nitrate|$`NO_3^-`$|
|Permanganate|$`MnO_4^-`$|
|Carbonate|$`CO_3^{2-}`$|
|Chromate|$`CrO_4^{2-}`$|
|Dichromate|$`Cr_2O_7^{2-}`$|
|Sulfate|$`SO_4^{2-}`$|
|Phosphate|$`PO_4^{3-}`$|

### Oxyanions
- Nitrate
- Borate
- Carbonate
- Chlorate
- Sulfate
- Phosphate
- And their family members :p.

## Deriving Ions From Parent

|Polyatomic Ion Name|Operation|Chemical Formula|
|:------------------|:--------|:-------|
|**Per**chlor**ate**|(add one extra oxygen to the parent)|$`ClO_4^-`$|
|Chlor**ate**|(**parent**)|**$`ClO_3^-`$**|
|Chlor**ite**|(has one less oxygen than the parent)|$`ClO_2^-`$|
|**Hypo**chlor**ite**|(has two less oxygens than the parent)|$`ClO^-`$|

- Note that the charge remains the same
- Polyatomic ions in the same group on the periodic table form similar polyatomic ions

|**Chlorate**|$`ClO_3^-`$|
|:-----------|:----------|
|Bromate|$`BrO_3^-`$|

## Acidic Oxyanions
- Acids generall have hydrogen ions $`(H^+)`$
- Acidic Oxyanions $`\rightarrow`$ Negatively charged ion with $`O`$ and $`H`$
- Each hydrogen added to a polyatomic ion increases the charge by one, and changes the name:

|Name|Chemical Formula|
|:---|:---------------|
|Hydrogen carbonate ion|$`HCO_3`$|
|Dihydrogen phosphate ion|$`H2PO_4^-`$|
|Monohydrogen phosphate ion|$`HPO_4^{2-}`$|
|Hydrogen Sulfate|$`HSO_4^-`$|
|Hydrogen Carbonate|$`HCO_3^-`$|

- For above, we use mono for phosphate to avoid ambigious cases, where $`H_2PO_4^{-}`$ and $`H_2PO_4^{2-}`$ are the same if we don't put `mono` infront. As for the Hyrogen carbonate ion we don't put a mono due to no ambigious cases. 

## Molecular Compounds
- Are not made of ions, instead molecules
- Shared pair of electrons -> `covalent bonds`
- `Lone pair` of electrons are electrons that are not shared
- Radicals are atoms with unpaired electrons, very reactive
- Molecules have **no charge**
- Atoms fill their valence shells to form molecules
- Double bond between oxygen atoms in an oxygen molecule

## Properties Of Ionic And Molecular Compounds
|Compound|State at Room Temperature|Solubility In Water|Colour of solution|Conductivity Of Solution|Ionic Or Molecular|
|:-------|:------------------------|:------------------|:-----------------|:-----------------------|:-----------------|
|ammonium chloride|solid|soluble, overtime the substance starts to get smaller and disappears|colourless|conductive|ionic|
|copper $`(II)`$ sulfate|solid|soluable|blue|conductive|ionic|
|sodium chloride|solid|soluble|colourless|conductive|ionic|
|calcium hydroxide|solid|slightly soluable|white|slightly conductive|ionic|
|sodium hydroxide|solid|soluble|colourless|conductive|ionic|
|sucrose|solid|soluble|colourless|not conductive|molecular|
|iodine|solid|not soluble|yellow|not conductive|molecular|
|hydrochloric acid|aqueous|soluble|colourless|conductive|molecular|
|ethanol|liquid|soluble|colourless|nont conductive|molecular|
|nitrogen gas|gas|N/A|N/A|N/A|molecular|
|carbon dioxide (dissolved in water)|gas|slightly soluble|colourless|a tiny bit conductive|molecular|

## Generalizations
|Classification of substances|Phase at room temperature|Solubility in water|Colour of solution|Conductivity of solution|
|:---------------------------|:------------------------|:------------------|:-----------------|:-----------------------|
|Ionic|Solid|Soluble|colourless, white|Conductive|
|Molecualr|liquid, gas, or solid|non-soluble|Has distinct colour?|Not really conductive|

## Binary Molecular Compounds
- 2 different kinds of atom in molecule
    - Eg. $`CO_2 \rightarrow`$ Carbon Diox**ide** $`\rightarrow`$ 2nd atom has `ide`.
    - $`CO \rightarrow`$ Carbon Monox**ide** $`\rightarrow`$ If 1st atom is mono, drop `mono`

### Greek Prefix For Number Of Atom
|Prefix|Name|Preifx|Name|
|:-----|:---|:-----|:---|
|1|mono|6|hexa|
|2|di|7|hepta|
|3|tri|8|octa|
|4|tetra|9|nona|
|5|penta|10|deca|

- `Diatomic Molecules` The **gens**, Hydrogen, Nitrogen, Oxygen, Halogen

### Common Names
- $`NH_3 \rightarrow`$ Ammonia
- $`H_2O \rightarrow`$ Water
- $`CH_4 \rightarrow`$ Methane

### Elements found As Molecules In Nature
- $`H_{2(g)}, Cl_{2(g)}, Br_{2(g)}, I_2, N_2, O_2, F_2`$

|Chemical Formula|Lewis Structure|What does the molecular model look like?|Name|
|:---------------|:-------------:|:--------------------------------------:|:---|
|$`H_2`$|<img src="http://www.chemspider.com/ImagesHandler.ashx?id=762&w=250&h=250" width="100">|<img src="https://cdn3.vectorstock.com/i/1000x1000/95/02/a-hydrogen-molecule-vector-20279502.jpg" width="100">|Hydrogen|
|$`O_2`$|<img src="https://qph.fs.quoracdn.net/main-qimg-bf33b3e696dfc2721cbfee1d8368ca9a.webp" width="200">|<img src="https://previews.123rf.com/images/molekuul/molekuul1409/molekuul140900149/31177823-elemental-oxygen-o2-molecular-model-atoms-are-represented-as-spheres-with-conventional-color-coding-.jpg" width="100">|Oxygen|
|$`N_2`$|<img src="https://qph.fs.quoracdn.net/main-qimg-13bfb554ead98ee9bd444b60454ae280.webp" width="100">|<img src="https://cdn4.vectorstock.com/i/thumb-large/28/98/molecular-formula-of-nitrogen-vector-19412898.jpg" width="200">|Nitrogen|
|$`I_2`$|<img src="https://us-static.z-dn.net/files/d17/0d510fa62a89479c97bf6307e6651ac3.png" width="200">|<img src="https://c8.alamy.com/comp/BKW3H5/iodmolekl-i2-iodine-molecule-i2-BKW3H5.jpg" width="200">|Iodine|
|$`H_2O`$|<img src="https://d2jmvrsizmvf4x.cloudfront.net/KfECaIsqRkKSnPW2pYHq_220px-Water-2D-flat.png" width="100">|<img src="https://ak7.picdn.net/shutterstock/videos/29270107/thumb/1.jpg" width="200">|Water|
|$`NH_3`$|<img src="https://study.com/cimages/multimages/16/ab9d659b-4e3a-4d72-b2e3-7a81ad0d18f1_ammonia.jpg" width="100">|<img src="https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2018/missinglinkf.jpg" width="100">|Ammonia|
|$`CO_2`$|<img src="http://nonsibihighschool.org/intbasch11_files/image015.png" width="200">|<img src="http://www.chm.bris.ac.uk/motm/CO2/CO2pic.gif" width="200">|Carbon dioxide|
|$`SBr_2`$|<img src="https://files.catbox.moe/61f8gw.jpg" width="200">|<img src="https://static.turbosquid.com/Preview/2016/04/01__14_28_24/01.jpg91e860b4-b1d4-432c-b6b2-62ee287c5a7cOriginal.jpg" width="100">|Sulfur dibromide|
|$`O_3`$|<img src="https://study.com/cimages/multimages/16/o3.jpg" width="200">|<img src="https://www.austindentalwellness.com/wp-content/uploads/2018/08/ozone-300x185.png" width="200">|Ozone|
|$`CF_4`$|<img src="http://mrsmittypapchem.weebly.com/uploads/3/8/0/2/38022025/8051199.gif?159" width="100">|<img src="https://www.chemtube3d.com/images/gallery/inorganicsjpgs/CF4.jpg" width="200">|Carbon tetrafluoride|
|$`SiH_4`$|<img src="https://study.com/cimages/multimages/16/229px-silane-sih4-2d.png" width="100">|<img src="http://www.3dchem.com/inorganics/SiH4.jpg" width="200">|Silicon tetrahydride|
|$`OH^-`$|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Hydroxide_lone_pairs-2D.svg/1200px-Hydroxide_lone_pairs-2D.svg.png" width="100">|<img src="http://www.chem.ucla.edu/~harding/IGOC/H/hydroxide_ion02.png" width="100">|Hydroxide ion|
|$`H_3O^+`$|<img src="http://davidjohnewart.com/Chemistry/chemtheft/text_images/FG17_01_01UN.JPG" width="100">|<img src="https://manditaylorjebe.files.wordpress.com/2011/09/h3o_1.jpg" width="100">|Hydrodium ion|

|Dots representing shared pairs of elections|Lines representing shared pairs of electrons|
|:-----------------------------------------:|:------------------------------------------:|
|<img src="https://us-static.z-dn.net/files/dda/2c1dcdc1c2156ae7d470b0daf0729abf.png" width="200">|<img src="https://www.thoughtco.com/thmb/R3AABuXGUgGOT3F7daTcl1tPFz0=/735x0/formaldehyde_LD-56a12a2c3df78cf772680353.png" width="200">|