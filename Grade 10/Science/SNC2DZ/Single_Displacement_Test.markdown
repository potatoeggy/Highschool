# Test Lab Results Single Displacement Test

## Activity 1

## Starting States
- $`Mg(NO_3)_2`$: A colourless seemingly clear liquid
- $`Zn(NO_3)_2`$: A colourless seemingly clear liquid
- $`Cu(NO_3)_2`$: A sky blue liquid     
- $`AgNO_3`$: A colourless seemingly clear liquid
- $`Mg`$: light gray in colour
- $`Cu`$: orangish brown in colour
- $`Zn`$: dark gray in colour
- $`Ag`$: silver, very light gray in colour

|Element/Compound|$`Mg(NO_3)_2`$|$`Zn(NO_3)_2`$|$`Cu(NO_3)_2`$|$`AgNO_3`$|
|:---------------|:-------------|:-------------|:-------------|:---------|
|$`Mg`$|N/A|The rod has now a `black coating` around it The solution has not changed much|The rod has now gotten a `light brownish` coating around the rod, the solution has gotten a tiny bit `lighter blue`.|The rod as now gotten a very light `gray coating` around the rod. Not much change in the solution.|
|$`Zn`$|Nothing changed|N/A|The solution has gotten a bit lighter, the `gray rod` now has a brownish coating around it|The rod now has a `silver/light gray` coating around it, nothing changed in the solution|
|$`Cu`$|Nothing changed|Nothing changed|N/A|The rod now has a `silver/gray` coating around it, and the solution turned `turquoise`|
|$`Ag`$|Nothing changed|Nothing changed|Nothing changed|N/A|


## Activity 2 

## Starting States
- $`Fe(NO_3)_2`$: A very light/white green coloured liquid
- $`Pb(NO_3)_2`$: A colourless seemingly clear liquid
- $`Ni(NO_3)_2`$: A grass/turquoise green coloured liquid
- $`Sn(NO_3)_2`$: A colourless seemingly clear liquid
- $`Fe`$: darkish gray in colour
- $`Pb`$: dark gray in colour
- $`Ni`$: light gray in colour
- $`Sn`$: light gray in colour

|Element/Compound|$`Fe(NO_3)_2`$|$`Pb(NO_3)_2`$|$`Ni(NO_3)_2`$|$`Sn(NO_3)_2`$|
|:---------------|:-------------|:-------------|:-------------|:---------|
|$`Fe`$|N/A|The solution turned a `medium dark green`, and there is now a `black` coating around the rod|The solution got a bit `darker`, and there is now a `silver/gray` coating around the rod|The solution has gotten a `greyish green`, the rod also now has a `silver/gray` coating|
|$`Pb`$|Nothing changed|N/A|Nothing changed|Nothing changed|
|$`Ni`$|Nothing changed|The solution turned into a `dark green`, and there is now a black coating around the rod|N/A|The solution has gotten a `greyish green`, and there is now a `silver/gray` coating around the rod| 
|$`Sn`$|Nothing changed|Nothing changed in solution, there is now a `dark thick black` coating around the rod|Nothing changed|N/A|

## Activity 3
## Starting States
- $`Fe(NO_3)_2`$: A very light/white green coloured liquid
- $`Zn(NO_3)_2`$: A colourless seemingly clear liquid
- $`Cu(NO_3)_2`$: A sky blue liquid
- $`Pb(NO_3)_2`$: A colourless seemingly clear liquid
- $`Fe`$: darkish gray in colour
- $`Cu`$: orangish brown in colour
- $`Zn`$: dark gray in colour
- $`Pb`$: dark gray in colour

|Element/Compound|$`Fe(NO_3)_2`$|$`Cu(NO_3)_2`$|$`Zn(NO_3)_2`$|$`Pb(NO_3)_2`$|
|:---------------|:-------------|:-------------|:-------------|:---------|
|$`Fe`$|N/A|There is now a `lightish brown/orangish` coating around the rod, the solution hasn't changed|Nothing changed|The solution now turned `light greyish` green, and there is now a `dark gray` coating around the rod|
|$`Cu`$|Nothing changed|N/A|Nothing changed|Nothing changed|
|$`Zn`$|The solution got more opaque, more `lighter green`, there is now a `silver` coating around the rod|The solution has gotten a lighter blue, and there is now a `orangish brownish` coating around the rod|N/A|Nothing changed in solution, there is now a `dark gray` coating around the rod| 
|$`Pb`$|Nothing changed|The solution got lighter, into a `lighter blue`, and there is now a `orangish brownish` coating around the rod|Nothing changed|N/A|

## Observations

- **Note:** The data tables are not ordered in a specific way, they are ordered in the order appeared in the website with some minor tweaks.
- By starting looking at the first table, we can see that Ag reacts with the least amount of solutions. Since this is an good observation, lets assume that the elements that reacts the least 
in each activity are the **strongest** elements, and the opposite be the **weakest** elements (**weak and strong right now have no real meaning**), lets order the elements and see 
how they vary:

### Activity 1 Ordered Strongest To Weakest
- $`Ag, Cu, Zn, Mg`$

### Activity 2 Ordered Strongest To Weakest
- $`Pb, Sn, Ni, Fe`$

### Activity 3 Ordered Strongest To Weakest
- $`Cu Pb, Fe, Zn`$

## Observations Cont
- By combining the above 3 lists, we can get a general list of whos **weaker** and whos **strong**, as listed below:
- $`Ag, Cu, Pb, Sn, Ni, Fe, Zn, Mg`$, 
- Also, we can look at the different colours each rod makes, Copper seems to make a orangish brown coating around the rod with **weaker** elements, Iron seems to make a greyish green solution with dark gray/gray coating with **wekaer** elements.
- Elements only react with a **weaker** element.
- We can also see that **weaker** elements are more reactive than stronger **elements**
- 



