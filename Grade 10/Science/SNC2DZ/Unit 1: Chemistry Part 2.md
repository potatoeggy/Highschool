# Chemistry

## Acids

### Naming Binary Acids
- $`HCL_{(g)} \rightarrow`$ hydrogen chloride
- $`HCL_{(aq)} \rightarrow`$ **hydro**fluoric **acid**
- Need to put the **hydro** prefix as it tells us its disolved in water and is an acid

## Oxyacids

### Naming Oxyacids
- `ite` ions make `ous` acides (suffix). $`HCLO_2 = `$ Chlor**ous** acid
- `ate` ions make `ic` acides (suffix). $`HCLO_3 = `$ Chlor**ic** acid
- Prefixes stay the same

## Chemical Change
- Similar to chemical reaction

## Evidence of Chemical Change: (observations that tell us its happening)
1. A new gas is formed (new odour, formation of bubbles)
2. A large change in energy (eg. Light, heat, sound, electricity)
3. A new colour is formed
4. A new solid is formed

## Representing Chemical Reactions
- A Chemical equation!
    - Used to model what is happening during a chemical reaction.
    - We use an arrow instead of an equal sign to show `beomces`, `reacts to form`, `produces`
    - On the **left** side, we always have the **`REACTANTS`**, (INPUT), these get used up in the chemical reaction
        - There can be plus signs to show multiple `reactants`. (Recipes, we are adding them together)
    - On the **right** side, we have the **`PRODUCTS**` (OUTPUT).
        - Newly produced/made from `reactants`  
        - Plus signs show multple products, more like an **AND** more than anything else
- In a chemical reaction, reactant, molecules/atoms/formula units/ions reaarange to produce products moleccules/atoms/ions/formula units
- eg. Wax $`+`$ oxygen gase $`\rightarrow`$ Soot $`+`$ Water $`+`$ Carbon dioxide. 
- A `word` equation 
- $`\triangle`$ Greek letter to represent heat.
- $`C_{25}H_{52(g)} + O_{2(g)} \rightarrow^\triangle C_{(g)} + H_2O_{(g)} + CO_{2(g)}`$

## Types Of Reactions

- 6 types of reactions
    - synthesis
    - decomposition
    - single displacement
    - double displacement
    - combustion
    - neutralization

## Sythensis
- When 2 or more substances combines into one substance
- $`A + B \rightarrow AB`$

## Decomposition
- With a substance breaks down into 2 or more substances
- $`AB \rightarrow A + B`$

## Single Diplacement
- When 

## Double Displacement

## Combustion

## Neutralization

