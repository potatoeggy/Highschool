# Vocab

- [Ecrivain](https://www.wordreference.com/enfr/writer): writer
- [écologiste](https://www.wordreference.com/enfr/environmentalist): Environmentalist
- [anthropology](https://www.wordreference.com/fren/anthropologie): anthropology
- [musulman, musulmane](https://www.wordreference.com/enfr/muslim): muslim
- [cinéaste](https://www.wordreference.com/enfr/filmmaker): filmmaker
- [critique de cinéma](https://www.wordreference.com/enfr/film%20critic): film critic
- [parolier, parolière](https://www.wordreference.com/fren/parolier): songwriter, lyricist
- [haïr, detester](https://www.wordreference.com/enfr/hate): hate, deteste
- [réduit](https://www.wordreference.com/enfr/impaired): impaired
- [marié](https://www.wordreference.com/enfr/married): married
- [médaillé, médaillée](https://www.wordreference.com/enfr/medalist): medalist
- [paralympien](https://www.wordreference.com/enfr/Paralympian): Paralympian
- [sénégalais](https://www.wordreference.com/enfr/Senegalese): Senegalese
- étiquette: Label
