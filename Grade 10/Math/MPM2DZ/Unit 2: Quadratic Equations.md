# Unit 2: Quadratic Equations

- $`\text{If } x \ge 0, x \in \mathbb{R}, \sqrt{x} \times \sqrt{x} = x`$

## Multiplication Of Radicals
- $`\text{If } a, b \ge 0, \text{ and } a, b, \in \mathbb{R}, \text{ then } \sqrt{a} \times \sqrt{b} = \sqrt{ab}`$
- There are **two** types of radical term,
1. An **entire radical** is in the form $`\sqrt{n}`$, where $`n`$ is the **radicand**.
2. A **mixed radical** is in the from $`a\sqrt{b}`$, where $`a`$ is the **rational factor** and $`b`$ is the **irrational factor**.

## Division Of Radicals
- $`\text{If } a \ge 0, b \gt 0, \text{ and } a, b, \in \mathbb{R}, \text{ then } \dfrac{\sqrt{a}}{\sqrt{b}} = \sqrt{\dfrac{a}{b}}`$

## RULES!
- $`(\sqrt{x})^2 = x, x \gt 0`$, remember, $`n`$ **must be positive** inorder for this equation to be **true**

- $`\sqrt{x^2} = |x|`$
- You can subtract like terms only if they the radicals have the same **irrational factor**. Eg $`2 \sqrt{7} + 5 \sqrt{7} = 7 \sqrt{7}`$

## Rationalizing Denominator
- Its not proper to leave radicals in the denominator, so we can multiply the denominator by it self, inorder to get rid of the radical. 

- Eg $`\dfrac{\sqrt{7}}{\sqrt{3}} = \dfrac{\sqrt{7}}{\sqrt{3}} \times \dfrac{\sqrt{3}}{\sqrt{3}} = \dfrac{\sqrt{21}}{3}`$

- Although sometimes, if there is 2 terms in the denominator, we can multiply it by its **conjugate**. Recall **difference of squares**, $`(a + b) \text{ and } (a-b)`$ are **conjugates** of one another.
- Then, the denominator becames a difference of squares, and we got rid of the radical.

- Eg $`\dfrac{1+\sqrt{3}}{1-\sqrt{3}} \times \dfrac{1+\sqrt{3}}{1+\sqrt{3}} = \dfrac{(1+\sqrt{3})^2}{1-3} = \dfrac{1+2\sqrt{3}+3}{-2} = -4-\sqrt{3}`$

## Introduction To Quadratic Equation
- The **standard form** of a quadraic is $`ax^2 + bx + c = 0`$, where $`a`$ is the **quadratic coefficient**, $`b`$ is the **linear coefficient**, and $`c`$ is the **constant coefficient**.
- You can solve a quadratic by factoring/decomposition, then applying the **Zero Factor Principle**, and solve for $`x`$. The **Zero Factor Principle** is if $`A \times B = 0`$, then either $`A = 0`$ or $`B = 0`$.

## Completing The Square
- This process is simply trying to create a perfect trinomial, while still balancing the equation/making the equation true
- The **Standard form** of a quadratic function $`y = ax^2+bx+c`$ can be rearranged to **Vertex form**, $`y = a(x-h)^2 + k`$ through completing the square, the **Vertex** $`(h, k)`$ can be eaisly read from this form.

### Steps To Complete The Square
- Factor out the $`a`$ coefficient from the first 2 terms. Make sure to put brackets around them.
- Add and subtract within the brackets $`(\dfrac{b}{2a})^2`$
- Remove the bracket from step 1 by applying **distrubutive property** (multiplying $`a`$/the **quadratic coefficient**)
- Factor the **perfect trinomial** that was created, and combine **like terms**.

### Solving Quadratic Equations By Completing The Square
- First complete the square of the quadratic equation/function.
- Move the constant terms to the other side.
- Square both sides. 
- Isolate $`x`$

## Quadratic Formula

```math
x = \dfrac{-b \pm \sqrt{b^2 - 4ac}}{2a} \\

\text{Where } ax^2+ bx + c = 0, a =\not 0, \text{ and } x \text{ are the roots of that quadratic equation}
```

- The formula is derived from completing the square.
- The **sums of the roots** is simply $`\dfrac{-b}{a}`$, or $`X_1 + X_2 = \dfrac{-b}{a}`$
- The **products of the roots**, is simply $`\dfrac{c}{a}`$, or $`(X_1)(X_2) = \dfrac{c}{a}`$
- The **Axis of Symmetry** is at $`\dfrac{-b}{2a}`$

## Discriminant
- $`D =b^2 - 4ac`$, this is also part of the **quadratic formula**!
- If $`D > 0`$, the quadratic equation has 2 distinct real roots
- If $`D = 0`$, the quadratic equation has 1 distinct real root or 2 equal real roots
- If $`D < 0`$, the quadratic equation has no real roots

|Two distinct real roots|One real root|No real roots|
|:---------------------:|:-----------:|:-----------:|
|$`b^2-4ac>0`$|$`b^2-4ac=0`$|$`b^2-4ac<0`$|

## Complex Numbers
- $`i = \sqrt{-1}`$. This equation has no solution in the set of real numbers
- An expression in the from $`a + bi`$, called the rectangular from, where $`a`$ and $`b`$ are real numbers, and $`i`$ is a complex number.
- The set of complex numbers includes the real numbers since any real number $`x`$ can be written as $`x + i(0)`$. 
- $`a+bi`$ and $`a-bi`$ are conjugates(same term with opposite signs).
- Complex roots of a quadratic quation occurs in **conjugate pairs**, recall discriminant, if its less than 0, there are 2 complex roots that are **conjugates** ($`a \pm bi`$)

|Complex Number|Equivalent|
|:--------------|:---------|
|$`i`$|$`\sqrt{-1}`$|
|$`i^2`$|$`-1`$|
|$`i^3`$|$`-\sqrt{-1}`$ or $`-i`$|
|$`i^4`$|$`1`$|

## Number Systems

<img src="https://www.shelovesmath.com/wp-content/uploads/2018/10/Venn-Diagram-of-Numbers.png" width="500">

- **Natural Numbers** $`\mathbb{N} = \{1,2,3, \cdots\}`$
- **Whole Numbers** $`\mathbb{W} = \{0, 1, 2, 3\cdots\}`$
- **Integers** $`(\mathbb{I}`$ or $`\mathbb{Z}) = \{\cdots, -2, -1, 0,1,2, \cdots\}`$
- **Rational numbers** $`(\mathbb{Q}) = \{\frac{a}{b}, a, b, \in \mathbb{I}, b =\not 0\}`$
- **Irrational Numbers** $`(\mathbb{Q} \prime)`$: any real number that cannot be written as $`\frac{a}{b}, a, b, \in \mathbb{I}, b =\not 0`$
- **Real Numbers** $`(\mathbb{R})`$: the set of $`\mathbb{Q} \cup \mathbb{Q} \prime`$
- **Complex Numbers** $`\mathbb{C}`$: any number that can be expressed in the form $`a+ib`$ (includes the set of real numbers) 


## Radical Equations
- `Extraneous Sol` $`\rightarrow`$ $`LS =\not RS`$
- `Inadmissable Sol` $`\rightarrow`$ Solutions you reject due to problem statement, eg negative length.
- `Extraneous values` occur because squaring both sides of an equation is not a reversible step.
- Make sure to check your work after working with radical equations, since squaring both sides is not a reversible step. Thus equations must be verified by pluging it back into the equation.
- **Radical Equations** are called that because the variable occurs under a radical sign. We **rationalize** the radical variable before continuing to slve the equation.
