**1. Solve for** $`x`$ **if** $`\dfrac{2x+3}{4} - \dfrac{3x-1}{3} = \dfrac{5x - 2}{2}`$

- $`3(2x+3) - 4(3x-1) = 6(5x-2)`$

- $`6x+9 - 12x + 4 = 30x - 12`$
- $`-6x + 13 = 30x - 12`$
- $`36x = 25`$
- $`x = \dfrac{25}{36}`$

**2. Determine the length of** $`AB`$ **for** $`\triangle ABC`$, **where** $`D`$ **is on segment** $`AC`$, and $`AD = 7, DC = 3,BC = 5`$
- $`\because 3^2 + 4^2 = 5^2 `$
- $`\therefore BD = 4`$
- $`\because \angle{ADB} = 90\degree`$
- $`\therefore BA = BD^2 + 7^2`$
- $`\therefore BA = \sqrt{4^2 + 7^2}`$
- $`BA = \sqrt{65}`$
- The length of $`AB`$ is $`\sqrt{65}`$

**3. Line 1 goes through the points** $`(-3, -7)`$ **and** $`(9, 1)`$. **Line 2 is perpendicular to** $`3x-4y+8 = 0`$ **and has a y-intercept of** $`7`$. **Determine the point of intersection of line 1 and line 2**.
- $`m_1 = \dfrac{1-(-7)}{9-(-3)}`$

- $`m_1 = \dfrac{8}{12}`$

- $`m_1 = \dfrac{2}{3}`$

- $`1 = \dfrac{2}{3}(9) + b`$

- $`1 = 6 + b`$

- $`b = -5`$

- $`y_1 = \dfrac{2}{3}x - 5`$

- $`3x - 4y + 8 = 0 `$

- $`4y = 3x + 8`$

- $`y = \dfrac{3}{4}x + 2`$

- $`m_{\perp} = \dfrac{-4}{3}`$

- $`y_2 = \dfrac{-4}{3}x + 7`$

- $`\begin{cases} y = \dfrac{-4}{3}x + 7 \quad (1) \\ \\y = \dfrac{2}{3}x - 5 \quad (2) \end{cases}`$ 

- **sub** $`(1)`$ **into** $`(2)`$

- $`\dfrac{2}{3}x - 5 = \dfrac{-4}{3}x + 7`$

- $`\dfrac{6}{3}x = 12`$

- $`2x = 12`$

- $`x = 6 (3)`$

- **sub** $`(3)`$ **into** $`(2)`$

- $`y = \dfrac{-24}{3} + 7`$

- $`y = -8 + 7`$

- $`y = -1`$

- $`\therefore`$ the Point of Intersection is $`(6, -1)`$ 

